<?php

return [
    // \App\Models\Ver2\Object3d::class,
    // \App\Models\Ver2\Bundle::class,
    // \App\Models\Ver2\Layout::class,
    // \App\Models\Ver2\Brand::class,
    // \App\Models\Ver2\Category::class,
    // \App\Models\Ver2\Color::class,
    // \App\Models\Ver2\ColorGroup::class,
    // \App\Models\Ver2\Vendor::class,
    // \App\Models\Ver2\Material::class,
    // \Plugin\CS\Models\Customer::class,

    \App\Models\Brand::class,
    \App\Models\Layout::class,
    \App\Models\Project::class,
    \App\Models\Object3d::class,
    \App\Models\Room::class,
    \App\Models\Category::class,
    \App\Models\Style::class,
    \App\Models\User::class,
    \App\Models\Material::class,
    \App\Models\Bundle::class,
    \App\Models\Color::class
];
