<?php

return [

    "default_asset_image" => env('STATIC_DEFAULT_IMAGE', "https://cdn.fitin.vn/images/3d-thumb.png") ,
    
    "master" => [
        'filesystem_driver' => env('MASTER_FILESYSTEM_DRIVER', 'master'),
        'static_path' => env('MASTER_STATIC_PATH', ''),
        'static_url'  => env('MASTER_STATIC_URL', ''),
        'private_token' => 'NT8SL3uWc1VBIpseZqv2'
    ],

    'static_user_agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',
    'errors' => [
        'auth' => [
            'inactive' => 10,
            'invalid' => 11
        ]
    ],

    "config" => [
        \App\Models\SystemConfig::TYPE_RESOURCE_CLASS => [
            [
                "name" => \App\Models\Material::CONFIG_NAME,
                "key" => \App\Models\Material::CONFIG_TYPE
            ],
            [
                "name" => \App\Models\Asset::CONFIG_NAME,
                "key" => \App\Models\Asset::CONFIG_TYPE
            ],
            [
                "name" => \App\Models\AssetStyleFit::CONFIG_NAME,
                "key" => \App\Models\AssetStyleFit::CONFIG_TYPE
            ]
        ]
    ]
    
];
