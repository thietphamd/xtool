<?php

return [

    [
        'model' => \App\Models\Ver2\Object3d::class,
        'type' => 'object',
        'actions' => [
            'object.create' => ['param' => false , 'description' => 'Create new object'],
            'object.update' => ['param' => false , 'description' => 'Update an object. If you pass a object key name, you can keep follow only this specific object'],
            'object.delete' => ['param' => false , 'description' => 'Delete an object. If you pass a object key name, you can keep follow only this specific object']
        ]
    ],

    [
        'model' => \App\Models\Ver2\Bundle::class,
        'type' => 'bundle',
        'actions' => [
            'bundle.create' => ['param' => false , 'description' => ''],
            'bundle.update' => ['param' => false , 'description' => ''],
            'bundle.delete' => ['param' => false , 'description' => '']
        ]
    ],

    [
        'model' => \App\Models\Ver2\Layout::class,
        'type' => 'layout',
        'actions' => [
            'layout.create' => ['param' => false , 'description' => ''],
            'layout.update' => ['param' => false , 'description' => ''],
            'layout.delete' => ['param' => false , 'description' => '']
        ]
    ],


];
