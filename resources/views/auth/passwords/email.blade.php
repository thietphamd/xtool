@extends('layouts.app')

@section('page')
<!-- START card-->
<div class="card card-flat">
  <div class="card-header text-center">
      <a href="/">
        <img class="block-center rounded" src="{{asset('img/logo.png')}}" alt="Image">
      </a>
  </div>
  <div class="card-body">
      <p class="text-center py-2">{{ __('Reset Password') }}</p>
      @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif
      <form method="POST" action="{{ route('password.email') }}">
          @csrf

          <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

              <div class="col-md-8">
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group row mb-0">
              <div class="col-md-8 offset-md-4">
                  <button type="submit" class="btn btn-primary">
                      {{ __('Send Password Reset Link') }}
                  </button>
              </div>
          </div>
      </form>
  </div>
</div>
@endsection
