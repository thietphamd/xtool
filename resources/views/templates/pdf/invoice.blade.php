<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Example</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" /> -->
</head>

<body>
    <div class="container mt-5">
        <h2 class="text-center mb-3">Invoice of Tenant {{$invoice['tenant']['name']}}</h2>

        <div class="mb-4">
            <p><b>Tenant: </b><span>{{($invoice['tenant']&& $invoice['tenant']['name']) ? $invoice['tenant']['name'] : 'N/A'}}</span></p>
            <p><b>Tenant Email: </b><span>{{($invoice['tenant'] && $invoice['tenant']['email']) ? $invoice['tenant']['email'] : 'N/A'}}</span></p>
            <p><b>Subscription: </b><span>{{( $invoice['payment'] && $invoice['payment']['subscription_plan'] && $invoice['payment']['subscription_plan']['name']) ? $invoice['payment']['subscription_plan']['name'] : 'N/A'}}</span></p>
            <p><b>Amount: </b><span>{{($invoice['payment'] && $invoice['payment']['price']) ? $invoice['payment']['price'] : '0'}}</span></p>
            <p><b>Recurring Month: </b><span>{{($invoice['payment'] && $invoice['payment']['recurring_month']) ? $invoice['payment']['recurring_month'] : '0'}}</span></p>
        </div>


    </div>

    <!-- <script src="{{ asset('js/app.js') }}" type="text/js"></script> -->
</body>

</html>