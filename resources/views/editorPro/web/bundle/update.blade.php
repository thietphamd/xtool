<!DOCTYPE html>
<html lang="en">
<head>
  <title>Fitin Home Styler</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Bổ sung thông tin Bundle</h2>
  <form id="bundle-info">
    <div class="form-group text-center">
      <img style="max-width:30%" src="{{$bundle->imageUrl}}"/>
    </div>
    <div class="form-group">
      <label for="email">Tên đại diện:</label>
      <input type="text" class="form-control" id="displayName" value="{{$bundle->displayName}}" placeholder="Nhập tên" name="displayName">
    </div>
    <div class="form-group">
      <label for="email">Mô tả:</label>
      <textarea style="resize:none" rows=3 class="form-control" id="description"  placeholder="Nhập vào miêu tả" name="description">{{$bundle->description}}</textarea>
    </div>
    <div class="form-group">
      <label for="tmpprice">Giá tạm tính:</label>
      <input type="text" class="form-control" disabled id="tmpprice" value="{{number_format($bundle->productList['total_price'], 0 ,',','.')}}" placeholder="Enter Price">
    </div>
    <div class="form-group">
      <label for="price">Giá thực:</label>
      <input type="text" class="form-control" id="price" value="{{$bundle->price}}" placeholder="Nhập vào giá" name="price">
    </div>
    <div class="form-group">
      <label for="pwd">Từ khóa:</label>
      <input type="text" class="form-control" id="keywords" value="{{$bundle->keywords}}" placeholder="Nhập từ khóa" name="keywords">
    </div>

    <div class="form-group">
      <label for="pwd">Danh sách sản phẩm:</label>
      <div class="clearfix">
                  <table class=" table table-striped table-bordered">
                    <tbody>
                    @foreach ($bundle->productList['items'] as $item)
                      <tr>
                        <td style="line-height:80px;width:15%"><img  style="max-height:80px;" src="{{$item['product']['thumbnail_url']}}"></td>
                        <td style="line-height:80px;width:30%"><b>{{$item['product']['object_key']}}</b></td>
                        <td style="line-height:80px"><b>{{$item['product']['name']}}</b></td>
                        <td style="line-height:80px"><b>{{number_format($item['product']['price'], 0 ,',','.')}} VNĐ</b></td>
                        <td style="line-height:80px;text-align:center">{{$item['count']}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
    </div>

    <button type="submit" class="btn btn-default">Cập nhật</button>
  </form>
</div>


<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="modalAlert" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        <button type="button" class="btn btn-primary"  data-dismiss="modal">Đóng</button>
      </div>
    </div>
  </div>
</div>

<script>
    (function ($) {
        $.fn.serializeFormJSON = function () {

            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);
   
    $( "#bundle-info" ).submit(function( event ) {
        event.preventDefault();
        var data = $(this).serializeFormJSON();
        console.log(data);
        $.ajax({
            url: '{{$api}}',
            type: 'PUT',
            contentType: 'application/json',
            headers: {
                'Authorization':'Bearer {{$token}}',
                'Content-Type':'application/json'
            },
            data: JSON.stringify(data), 
            success: function(response) {
                //...
                console.log(response);
                if(response.code == 0){
                    $("#modalAlert .modal-body").html('Cập nhật thành công!');
                    $("#modalAlert").modal('show');
                }else{
                    $("#modalAlert .modal-body").html(response.message);
                    $("#modalAlert").modal('show');
                }
            },
            fail: function(response) {
                //...
                if(response.message){
                    $("#modalAlert .modal-body").html(response.message);
                    $("#modalAlert").modal('show');
                }else{
                    $("#modalAlert .modal-body").html('Đã có lỗi xảy ra');
                    $("#modalAlert").modal('show');
                }
                
            },
        });
    });

    (function($, undefined) {

        "use strict";

        // When ready.
        $(function() {
            
            var $form = $( "#bundle-info" );
            var $input = $form.find( "input[name=price]" );

            $input.on( "keyup", function( event ) {
                
                
                // When user select text in the document, also abort.
                var selection = window.getSelection().toString();
                if ( selection !== '' ) {
                    return;
                }
                
                // When the arrow keys are pressed, abort.
                if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                    return;
                }
                
                
                var $this = $( this );
                
                // Get the value.
                var input = $this.val();
                
                var input = input.replace(/[\D\s\._\-]+/g, "");
                        input = input ? parseInt( input, 10 ) : 0;

                        $this.val( function() {
                            return ( input === 0 ) ? "" : input.toLocaleString( "vi-VN" );
                        } );
            } );
            
            /**
             * ==================================
             * When Form Submitted
             * ==================================
             */
            $form.on( "submit", function( event ) {
                
                var $this = $( this );
                var arr = $this.serializeArray();
            
                for (var i = 0; i < arr.length; i++) {
                        arr[i].value = arr[i].value.replace(/[($)\s\._\-]+/g, ''); // Sanitize the values.
                };
                
                console.log( arr );
                
                event.preventDefault();
            });
            
        });
    })(jQuery);
</script>
</body>
</html>
