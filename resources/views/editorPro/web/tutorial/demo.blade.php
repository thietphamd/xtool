<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='shortcut icon' type='image/x-icon' href='img/favicon.ico' />
    <title>Fitin Pro</title>
    <!--
Neaty HTML Template
http://www.templatemo.com/tm-501-neaty
-->
    <!-- load stylesheets -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Google web font "Open Sans" -->
    <link rel="stylesheet" href="/editor/css/bootstrap.min.css"> <!-- Bootstrap style -->
    <link rel="stylesheet" href="/editor/css/magnific-popup.css">
    <!-- Magnific pop up style, http://dimsemenov.com/plugins/magnific-popup/ -->
    <link rel="stylesheet" href="/editor/css/templatemo-style.css"> <!-- Templatemo style -->
    <link rel="stylesheet" href="/editor/css/custom.css">     
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
</head>

<body>
    <div class="container demo">
        <div class="row">
            <div class="tm-left-right-container">
                <!-- Left column: logo and menu -->
                <div class="tm-black-bg tm-left-column">
                    <div class="tm-logo-div text-xs-center">
                        <!-- <img src="/editor/img/fitin.png" alt="Logo"> -->
                        <h1 class="tm-site-name">Fitin Pro</h1>
                    </div>
                    <nav class="tm-main-nav">
                        <ul class="tm-main-nav-ul">
                            <li class="tm-nav-item">
                                <a href="#general" class="tm-nav-item-link">General</a>
                            </li>
                              
        
                            <!-- <li class="tm-nav-item">
                                    <a href="#tutorial4" class="tm-nav-item-link">Third Gallery</a>
                                </li> -->
                            <li class="tm-nav-item">
                                <a href="#tutorial" class="tm-nav-item-link">Tutorial</a>
                            </li>
                        
                            <li class="tm-nav-item demo-menu">
                                <a href="#demo" class="tm-nav-item-link demo-link">Demo</a>
                            </li>
                        </ul>
                    </nav>
                </div> <!-- Left column: logo and menu -->

                <!-- Right column: content -->
                <div class="tm-right-column demo-content">
                    <div id="demo">
                        <iframe frameborder="0" allowfullscreen
                                style="width:100%;height:100%;" id="demo-iframe" src=""></iframe>
                    </div>
                </div>

                <div class="tm-right-column normal-content">
                    <figure>
                        <img src="/editor/img/8.png" alt="Header image" class="img-fluid banner">
                    </figure>

                    <div class="tm-content-div">
                        <!-- Welcome section -->
                        <section id="general" class="tm-section">
                            <header>
                                <h3 class="tm-black-text tm-margin-b-45">Provide users an easy-to-use 2d/3d home design tool</h3>
                            </header>
                            <div class="ak-renderer-document">

                            
                               
                                <ol class="ak-ol" style="list-style-type: none;">
                                    <li>
                                        <p><strong>Design floor plan</strong>:  Use the 2D mode to create floor plans and design layouts with furniture and other home items, or switch to 3D to explore and edit your design from any angle. Provide thousand floor-plans to shorten the design time. </p> 
                                        
                                    </li>
                                    
                                    <li class="mt-1">
                                        <p><strong>Furnish & Decorate </strong>:  Able to colors, materials, finished to create unique furniture, walls, floors. Even adjust item sizes to find the perfect fit. Provide a library of world-brands furniture.
                                         </p>   
                                    </li>
                                </ol>
                            
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                            </div>
                            
                        </section>
                        
                        

                        <!-- Second Gallery section -->
                        <section id="tutorial" class="tm-section">
                            <header>
                                <h3 class="tm-black-text tm-margin-b-30">Getting start with Fitin Pro Web</h3>
                            </header>
                            <div class="ak-renderer-document">
                                <p>Welcome to Fitin Pro Web,</p>
                                <p>This short tutorial will give you a brief overview of how to start working with Fitin Pro Web and easily design floor plans.</p>
                            </div>    
                            <header class="mt-2">
                                <h4 class="tm-black-text tm-margin-b-30">Tutorial</h4>
                            </header>
                            <div class="ak-renderer-document">
                                <p>The first step is to log into <a href="https://pro-demo.fitin.vn/">https://pro-demo.fitin.vn/</a> . Don't worry, it is just a demo and free to use. Here is demo account :</p>
                                <ul class="ak-ol"> 
                                    <li>
                                        <p>Username : demofitin@gmail.com</p>
                                    </li>
                                    <li>
                                        <p>Password : demo@1357</p>
                                    </li>
                                </ul>
                                <p>At home screen, Click “<b>Floor Plan</b>” to choose which room you want to design. Tool provides many default floor plans.</p>

                                <div class="mt-1">
                                    <img style="width:90%" src="/editor/img/floorplan.PNG"/> 
                                </div>

                                <p class="mt-1">After choosing a floor plan, you are able to design on that.</p>
                                <p>Here is a short description of the different functions you find in the toolbars.</p>

                                <table>
                                    <tr><td><img style="width:100px" src="/editor/img/function-1.png"></td><td><p><b>Catalog</b></p><p>You can choose furniture from many categories in Fitin Pro Library. </p></td></tr>
                                    <tr><td><img style="width:100px" src="/editor/img/function-2.png"></td><td><p><b>Material</b></p><p>You can choose material/ finished to design ceiling, wall,floor. </p></td></tr>
                                    <tr><td><img style="width:100px" src="/editor/img/function-3.png"></td><td><p><b>Split</b></p><p>Here you can split the ceiling, wall, floor to many different areas. </p></td></tr>
                                    <tr><td><img style="width:100px" src="/editor/img/function-4.png"></td><td><p><b>Summary</b></p><p>This tab will show the total cost for designing the floorplan. </p></td></tr>
                                    <tr><td><img style="width:100px" src="/editor/img/function1-1.png"></td><td><p><b>Separate Area </b></p><p>You can assign the area on the floor plans. </p></td></tr>
                                    <tr><td><img style="width:100px" src="/editor/img/function1-2.png"></td><td><p><b>2D View</b></p><p>You can view the design in 2d mode from the top. </p></td></tr>
                                    <tr><td><img style="width:100px" src="/editor/img/function1-3.png"></td><td><p><b>3D View</b></p><p>You can view the design in 3d mode from any angle. </p></td></tr>
                                    <tr><td><img style="width:100px" src="/editor/img/function1-4.png"></td><td><p><b>Walk View</b></p><p>You can view the design in first person view mode. </p></td></tr>
                                </table>
                                <br>
                                <p class="mt-1">Next step, you have easy-to-use tool to design your layout</p>
                                <table class="mt-1">
                                    <tr><td style="width:60%"><img style="width:100%" src="/editor/img/ceiling1.png"></td><td><p><b>Ceiling </b></p><p>You are able to change material / finished and add more features about the plaster ceiling. </p></td></tr>
                                    <tr><td><p><b>Wall </b></p><p>You are able to change color/ wallpaper/ pattern brick. </p></td><td style="width:60%"><img style="width:100%" src="/editor/img/wallpaper2.png"></td></tr>
                                    <tr><td style="width:60%"><img style="width:100%" src="/editor/img/floormat1.png"></td><td><p><b>Floor </b></p><p>You are able to change floor type : wood, brick. You are able to separate areas. </p></td></tr>
                                </table>

                                <p class="mt-2"><strong>Split:</strong> This is an advanced feature, you can split ceiling, wall, floor to many pieces. And add material/ finished on it.</p>
                                <ul class="ak-ol" style="list-style-type: decimal">
                                    <li>
                                        <p>Click “Split” and select ceiling, wall, floor.</p>
                                    </li>
                                    <li>
                                        <p>Enable “Cut” and drag to draw a plane.</p>
                                    </li>
                                    <li>
                                        <p>Add material to the plane.</p>
                                    </li>
                                    <li>
                                        <p>View result after you draw.</p>
                                    </li>
                                </ul>

                                <table>
                                    <tr><td style="width:50%"><img style="width:100%" src="/editor/img/split1.png"></td><td style="width:50%"><img style="width:100%" src="/editor/img/split2.png"></td></tr>
                                    <tr><td style="width:50%"><img style="width:100%" src="/editor/img/split3.png"></td><td style="width:50%"><img style="width:100%" src="/editor/img/split4.png"></td></tr>
                                </table>

                                <p><b>Separate area </b></p>
                                <ul class="ak-ol">
                                    <li class="row">
                                        <p class="col-lg-2">- Click on</p>
                                        <div><img src="/editor/img/function1-1.png"></div>
                                    </li>
                                    <li class="row">
                                        <p class="col-lg-4">- Set size and position of the area  </p>
                                    </li>
                                    <li class="row">
                                        <p class="col-lg-4">- Naming the area</p>
                                    </li>
                                    
                                </ul>
                                <div class="row">
                                    <div class="col-lg-6"><img width="100%" src="/editor/img/screenshot1.png"></div>
                                    <div class="col-lg-6"><img width="100%" src="/editor/img/screenshot2.png"></div>
                                </div>

                                <p class="mt-1"><b>Furnish & Decorate </b></p>
                                <div class="row">
                                    <div class="col-lg-6 vertical-align" style="margin-top:100px">
                                        <p>- Items able stick on the ceiling, hang on the wall, lay on the floor.</p>
                                        <p>- Able to drag and drop furniture & decoration.</p>
                                        <p>- Able to move/rotate.</p>
                                    </div>
                                    <div class="col-lg-6"><img width="100%" src="/editor/img/drag-drop.png"></div>
                                </div>
                                <div class="row  mt-1">                                
                                    <div class="col-lg-6"><img width="100%" src="/editor/img/swap.png"></div>
                                    <div class="col-lg-6 vertical-align" style="margin-top:100px">
                                        <p>- Able to swap the relative item.</p>
                                    </div>
                                </div>
                                <div class="row  mt-1">
                                    <div class="col-lg-6 vertical-align" style="margin-top:100px">
                                        <p>- Able to view real-world sizes of furniture.</p>
                                    </div>
                                    <div class="col-lg-6"><img  width="100%" src="/editor/img/measure.png"></div>
                                </div>

                                <div class="row mt-1">
                                    <div class="col-lg-6"><img  width="100%" src="/editor/img/screenshot10.png"></div>
                                    <div class="col-lg-6"><img   width="100%" src="/editor/img/screenshot11.png"></div>
                                </div>
                                <p>- Able to change color/finished of specific items(kitchen).</p>
                                <br>
                                <p class="mt-2"><b>Control & Shortcuts </b></p>
                                <div>
                                    <img  width="100%" src="/editor/img/tutorial.png"/>
                                </div>
                                <p>Enjoy planning with Fitin Pro Web!</p>
                            </div>
                            
                        </section>

                        <!-- Third Gallery section -->
                        <!-- <section id="tutorial4" class="tm-section">
                                <header><h2 class="tm-black-text tm-section-title tm-margin-b-30">Third Gallery</h2></header>
                                <div class="tm-gallery-container tm-gallery-3">
                                    <div class="tm-img-container tm-img-container-3">
                                        <a href="img/neaty-11.jpg"><img src="img/neaty-11.jpg" alt="Image" class="img-fluid tm-img-tn"></a>    
                                    </div>
                                    <div class="tm-img-container tm-img-container-3">
                                        <a href="img/neaty-12.jpg"><img src="img/neaty-12.jpg" alt="Image" class="img-fluid tm-img-tn"></a>
                                    </div>
                                    <div class="tm-img-container tm-img-container-3">
                                        <a href="img/neaty-13.jpg"><img src="img/neaty-13.jpg" alt="Image" class="img-fluid tm-img-tn"></a>
                                    </div>
                                    <div class="tm-img-container tm-img-container-3">
                                        <a href="img/neaty-14.jpg"><img src="img/neaty-14.jpg" alt="Image" class="img-fluid tm-img-tn"></a>
                                    </div>                                    
                                </div>
                            </section> -->

                        
                        <!-- Contact Us section -->
                        <!-- <section id="contact" class="tm-section">
                                <header><h2 class="tm-black-text tm-section-title tm-margin-b-30">Contact Us</h2></header>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <form action="#contact" method="post" class="contact-form">
                                            <div class="form-group">
                                                <input type="text" id="contact_name" name="contact_name" class="form-control" placeholder="Name"  required/>
                                            </div>
                                            <div class="form-group">
                                                <input type="email" id="contact_email" name="contact_email" class="form-control" placeholder="Email"  required/>
                                            </div>
                                            <div class="form-group">
                                                <textarea id="contact_message" name="contact_message" class="form-control" rows="9" placeholder="Message" required></textarea>
                                            </div>                                            
                                            <button type="submit" class="float-right tm-button">Send</button>
                                        </form>    
                                    </div>
                                    
                                    <div class="col-lg-6 tm-contact-right">
                                        <p>
                                        Nullam vivera fermentum purus id blandit. Phasellus lacus mi, porta vel sodales nec, faucibus non eros. Nulla at quam vel risus laoreet tincidunt in in sem.    
                                        </p>
                                        <address>
                                            11/22 Etiam mauris erat,<br>
                                            Vestibulum eu augue nec, 10220<br>
                                            Nam consequat
                                        </address>
                                    </div>
                                </div>
                                
                            </section> -->
                        <!-- <footer>
                            <p class="tm-copyright-p">Copyright &copy; <span class="tm-current-year">2019</span> Công Ty
                                Cổ Phần Công nghệ Fitin

                                | HomeStyler Pro Team</p>
                            <address>
                                Văn phòng đại diện: Lầu 8 Tòa nhà Le Meridien, 3C Tôn Đức Thắng, P. Bến Nghé, Quận 1,
                                TP. Hồ Chí Minh </br>
                                Văn phòng liên hệ: Lầu 8 Tòa nhà Ree Tower, 9 Đoàn Văn Bơ, Phường 12, Quận 4, TP. Hồ Chí
                                Minh
                            </address>
                        </footer> -->
                    </div>

                </div> <!-- Right column: content -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->

    <!-- load JS files -->
    <script src="/editor/js/jquery-1.11.3.min.js"></script> <!-- jQuery (https://jquery.com/download/) -->
    <script src="/editor/js/jquery.magnific-popup.min.js"></script>
    <!-- Magnific pop-up (http://dimsemenov.com/plugins/magnific-popup/) -->
    <!-- <script src="/editor/js/jquery.singlePageNav.min.js"></script> -->
    <!-- Single Page Nav (https://github.com/ChrisWojcik/single-page-nav) -->
    <script>
        var iframe_url = "https://api-editor.fitin.vn/unity/index.html";
        //var iframe_url = "http://localhost:9020/unity/index.html";
        $('.demo-content').hide();
        $(document).ready(function () {
            
            // Magnific pop up
            $('.tm-gallery-1').magnificPopup({
                delegate: 'a', // child items selector, by clicking on it popup will open
                type: 'image',
                gallery: { enabled: true }
                // other options
            });

            $('.tm-gallery-2').magnificPopup({
                delegate: 'a', // child items selector, by clicking on it popup will open
                type: 'image',
                gallery: { enabled: true }
                // other options
            });

            $('.tm-gallery-3').magnificPopup({
                delegate: 'a', // child items selector, by clicking on it popup will open
                type: 'image',
                gallery: { enabled: true }
                // other options
            });

            $('.tm-current-year').text(new Date().getFullYear());




            $('.tm-nav-item').on('click', function(){
                var className = $( this ).attr('class') ;
                $('.tm-nav-item a').removeClass('active');
                $(this).find('a').addClass('active');

                var id = $(this).find('a').attr('href');
                if(id){
                    $('html, body').animate({
                        scrollTop: $(id).offset().top
                    }, 500);
                }
                

                if(className.includes('demo-menu')){
                    $('#demo-iframe').attr('src', iframe_url);
                    $('.demo-content').show();
                    $('.normal-content').hide();
                }else{
                    $('.demo-content').hide();
                    $('.normal-content').show();
                }
            });

        });

        
        

    </script>
</body>


</html>