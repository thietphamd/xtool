// url api
console.log(process.env);
export const ApiBase = (process.env.MIX_APP_URL || '') + '/api/v1/';
export const ApiLayout = ApiBase + 'layouts';
export const ApiProject = ApiBase + 'projects';
export const ApiStyle = ApiBase + 'styles';
export const ApiCat = ApiBase + 'categories';
export const ApiObject = ApiBase + 'objects';
export const ApiUpload = ApiBase + 'uploads/image';
export const ApiUser = ApiBase + 'users';

// url web
export const URLPublic = (process.env.MIX_APP_URL || '');
export const URLHome = (process.env.MIX_APP_URL || '') + '/';
export const URLLayout = (process.env.MIX_APP_URL || '') + '/layout';
export const URLEditor = (process.env.MIX_APP_URL || '') + '/editor';
export const URLProject = (process.env.MIX_APP_URL || '') + '/project';
export const URLStyle = (process.env.MIX_APP_URL || '') + '/style';
export const URLBuilder = (process.env.MIX_APP_URL || '') + '/unity/index.html';
export const URLImgAssets = (process.env.MIX_APP_URL || '') + '/img/';
export const TemplateRoom = (process.env.MIX_APP_URL || '') + '/templateRoom';
