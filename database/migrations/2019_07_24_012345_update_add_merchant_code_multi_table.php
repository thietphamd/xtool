<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAddMerchantCodeMultiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('layouts', function (Blueprint $table) {
            $table->string('merchant_code')->nullable();
        });
        Schema::table('objects', function (Blueprint $table) {
            $table->string('merchant_code')->nullable();
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->string('merchant_code')->nullable();
        });
        Schema::table('sku', function (Blueprint $table) {
            $table->string('merchant_code')->nullable();
        });
        Schema::table('bundles', function (Blueprint $table) {
            $table->string('merchant_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bundles', function (Blueprint $table) {
            $table->dropColumn('merchant_code');
        });
        Schema::table('sku', function (Blueprint $table) {
            $table->dropColumn('merchant_code');
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('merchant_code');
        });
        Schema::table('objects', function (Blueprint $table) {
            $table->dropColumn('merchant_code');
        });
        Schema::table('layouts', function (Blueprint $table) {
            $table->dropColumn('merchant_code');
        });
    }
}
