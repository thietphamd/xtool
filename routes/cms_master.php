<?php

use Illuminate\Http\Request;
use App\Helpers\Utils;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('test', 'TestController@index');

Route::group([
    'prefix' => 'management/v1',
], function () {
   
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/logout', 'AuthController@logout');
    
    Route::group(['middleware' => [
         'auth:master'  // 'jwt.auth' 
        ]], function () {
            Route::get('auth/me', 'AuthController@me');
            Route::post('auth/password', 'AuthController@updatePassword');
            //Route::post('auth/profile', 'AuthController@updateProfile');
            //Route::post('auth/avatar', 'AuthController@uploadAvatar');
            
            //Route::post('upload/image', 'UploadController@uploadImage');
            Route::ApiResource('tenants', 'TenantController');
            Route::ApiResource('applications', 'SubscriptionApplicationController');
            Route::ApiResource('plans', 'SubscriptionPlanController');
            Route::post('plans/{plan_id}/options', 'SubscriptionPlanController@storeOption');
            Route::put('plans/{plan_id}/options/{option_id}', 'SubscriptionPlanController@updateOption');
            Route::delete('plans/{plan_id}/options/{option_id}', 'SubscriptionPlanController@destroyOption');
            Route::ApiResource('payments', 'PaymentController');
            Route::ApiResource('invoices', 'InvoiceController');
            Route::get('invoices/{id}/download/pdf', 'InvoiceController@downloadPDF');
            
            // System
            Route::get('system/types', 'SystemConfigController@getListTypes');
            Route::get('system/classes', 'SystemConfigController@getListClasses');
            Route::ApiResource('system/config', 'SystemConfigController');

            //Request
            Route::get('requests', 'AssetRequestController@index');
            Route::put('requests/{id}/reject', 'AssetRequestController@updateRejected');
            Route::put('requests/{id}/finish', 'AssetRequestController@updateFinished');
            Route::get('requests/{id}', 'AssetRequestController@show');
            Route::delete('requests/{id}', 'AssetRequestController@destroy');

            
    });


    Route::get('config/build/latest', 'ConfigController@getBuildConfigLatest')->middleware('editor-access-key');
    Route::get('config/build', 'ConfigController@getBuildConfig')->middleware('editor-access-key');
});

