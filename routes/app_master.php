<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'app-master/v1',
], function () {
   
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/logout', 'AuthController@logout');

    Route::group(['middleware' => [
         'auth:master'  // 'jwt.auth' 
        ]], function () {
            Route::get('auth/me', 'AuthController@me');

            //Select Tenant
            Route::post('auth/tenant', 'AuthController@grantTenantAuth');

            Route::get('requests', 'AssetRequestController@index');
            Route::put('requests/{id}/reject', 'AssetRequestController@updateRejected');
            Route::put('requests/{id}/finish', 'AssetRequestController@updateFinished');
            Route::get('requests/{id}', 'AssetRequestController@show');
            Route::delete('requests/{id}', 'AssetRequestController@destroy');

            //Tenant
            Route::ApiResource('tenants', 'TenantController');

            //Material
            Route::get('materials/categories', 'MaterialController@getCategories');
            Route::get('materials/brands', 'MaterialController@getBrands');
            Route::ApiResource('materials', 'MaterialController');
            Route::post('materials/upload/thumb', 'MaterialController@uploadThumb');
            Route::post('materials/upload/extensions/ab', 'MaterialController@uploadAssetBundle');
            Route::post('materials/upload/extensions/zip', 'MaterialController@uploadZip');
            Route::post('materials/upload/extensions/glb', 'MaterialController@uploadGlb');

            //Asset
            Route::get('assets/categories', 'AssetController@getCategories');
            Route::get('assets/brands', 'AssetController@getBrands');
            Route::ApiResource('assets', 'AssetController');
            Route::post('assets/upload/thumb', 'AssetController@uploadThumb');
            Route::post('assets/upload/extensions/ab', 'AssetController@uploadAssetBundle');
            Route::post('assets/upload/extensions/zip', 'AssetController@uploadZip');
            Route::post('assets/upload/extensions/glb', 'AssetController@uploadGlb');

            //Asset Style Fit
            Route::get('assets-stylefit/categories', 'AssetStyleFitController@getCategories');
            Route::get('assets-stylefit/brands', 'AssetStyleFitController@getBrands');
            Route::ApiResource('assets-stylefit', 'AssetStyleFitController');
            Route::post('assets-stylefit/upload/thumb', 'AssetStyleFitController@uploadThumb');
            Route::post('assets-stylefit/upload/extensions/ab', 'AssetStyleFitController@uploadAssetBundle');
            Route::post('assets-stylefit/upload/extensions/zip', 'AssetStyleFitController@uploadZip');
            Route::post('assets-stylefit/upload/extensions/glb', 'AssetStyleFitController@uploadGlb');

    });

});

