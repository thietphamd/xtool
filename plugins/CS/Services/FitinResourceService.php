<?php
namespace Plugin\CS\Services;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Log;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
class FitinResourceService
{
    protected $api_endpoint, $ecom_token;

    protected $api_url = [
        'customer' => 'api/frontend/v1/customers',
        
    ];

    public function __construct(){
        $this->api_endpoint = config('fitin.ecom.api_endpoint');
        $this->ecom_token =  config('fitin.ecom.private_token');
    }

    protected function getUrl($action){
        return $this->api_endpoint . '/' . $this->api_url[$action];
    }

    public function getCustomer($input){     
        $url = $this->getUrl('customer');
        $data = $this->requestGET($url, $input);
        return $data['data'] ?? $data; 

    }

    protected function requestGET($url, $query = false)
    {
        $arrData = [
            'headers' => [
                'Token' => $this->ecom_token
            ],
            'query' => $query ?  $query : []
        ];
        $client = new Client();
        $response = $client->request('GET',  $url , $arrData);
        $stream = $response->getBody();
        $contents = $stream->getContents();
        $result = json_decode($contents, 1);
        return $result;
    }
}
