<?php

namespace Plugin\CS\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TicketResourceCollection extends ResourceCollection
{
    public $collects = 'Plugin\CS\Http\Resources\TicketResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
                  
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,     
            'message' => 'Success', 
        ];
    }
}
