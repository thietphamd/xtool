<?php

namespace Plugin\CS\Http\Controllers;

use Plugin\CS\Models\Customer;
use Plugin\CS\Models\Ticket;
use Plugin\CS\Models\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Plugin\CS\Services\ResourceService;
use Plugin\CS\Http\Resources\SearchResource;

class ResourceController extends ApiController
{
    protected $resource_service;

    public function __construct(ResourceService $resource_service){
        $this->resource_service = $resource_service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $query = $request->get('query');
        $data = $this->resource_service->search($query);
        
        return new SearchResource($data);
    }

}
