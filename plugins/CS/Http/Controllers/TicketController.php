<?php

namespace Plugin\CS\Http\Controllers;


use Plugin\CS\Http\Resources\TicketResource;
use Plugin\CS\Http\Resources\TicketResourceCollection;
use Illuminate\Support\Facades\Validator;
use Plugin\CS\Models\Ticket;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

use Plugin\CS\Repositories\TicketRepository;
use Plugin\CS\Repositories\OrderRepository;

class TicketController extends ApiController
{

    protected $repository, $orderRepository;

    public function __construct(TicketRepository $repository, OrderRepository $orderRepository){
        $this->repository = $repository;
        $this->orderRepository = $orderRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $tickets = $this->repository->getAll();
        return new TicketResourceCollection($tickets);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'phone'   => 'required',
            'callReference' => 'required',
            'type' => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('type, phone, callReference required', "INVALID");   
        }
        $ticket = $this->repository->create($input);
        return new TicketResource($ticket);
    }
  
    public function createTicketFromFitin(Request $request)
    {
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'type' => 'required',
            'customer.phone'   => 'required',
            'order' => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('type, customer info and order required', "INVALID");   
        }
        $input['creator'] = 'fitin';
        $input['phone'] = $input['customer']['phone'];
        
        if(isset($input['order']) && isset($input['order']['id'])){
            $input['orderIds'] = [$input['order']['id']];
            $order = $this->orderRepository->firstOrCreate($input['order']);
        }
        $ticket = $this->repository->create($input);
        return new TicketResource($ticket);
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        $ticket = $this->repository->find($key);
        if(!$ticket){
            return $this->responseError('Ticket not found');
        }
        return new TicketResource($ticket);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input = $request->only(['type', 'description'. 'content']);
        $ticket = $this->repository->update($key, $input);
        if(!$ticket){
            return $this->responseError('Ticket not found');
        }
        return new TicketResource($ticket);
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $ticket = $this->repository->delete($key);
        if(!$ticket){
            return $this->responseError('Ticket not found');
        }
        return $this->respondSuccess();
    }
}
