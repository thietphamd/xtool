<?php

namespace Plugin\CS\Http\Controllers;

use Plugin\CS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Exceptions\GeneralException;


/**
 * @group 1. Authentication
 */
class AuthController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    public function __construct()
    {
       
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');
        $token = null;
        try { 
           if (Auth::guard('cs')->attempt($credentials)) {              
                $token = JWTAuth::fromUser(Auth::guard('cs')->user());
                return response()->json([
                    'code' => 0,
                    'data' => [
                        'access_token' => $token
                    ],
                    'message' => "Sucess"
                ]);
           }else{
            return $this->responseError('Unauthorized!', 'UnauthorizedException', -401);
           }

        } catch (JWTAuthException $e) {
            return $this->respondError($e->getMessage());
        }
    }


    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();
        return $this->respondSuccess(__('Successfully logged out'));
    }



    

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('cs');
    }


}
