<?php

namespace Plugin\CS\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;
use Plugin\CS\Traits\SearchTrait;

class Customer extends Eloquent {

    use CacheTrait, SearchTrait;
    
    protected $collection = 'CSCustomers';

    const INDEXES = ['phone'];

    protected $fillable = [

        "name",
        "phone",
        "2ndphone",
        "email",
        "type",
        "userId"
    ];

    protected $searchable = [
        'email',
        'phone',
        '2ndphone'
    ];
    
    public const CACHE_TAG = 'cs-customer';
    public const ADDITIONAL_CACHE_TAG = 'cs-search';
    
    public function tickets(){
        return $this->hasMany(Ticket::class, 'phone', 'phone')->latest();
    }

    public function orders(){
        return $this->hasMany(Order::class, 'userId', 'userId')->latest();
    }

    public function setPhoneAttribute($value){
        $this->attributes['phone'] = (string) $value;
    }
}