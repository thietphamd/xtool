<?php

namespace Plugin\CS\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
//use Illuminate\Contracts\Auth\MustVerifyEmail;
//use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Authenticatable;

use Jenssegers\Mongodb\Eloquent\Model;

//use Jenssegers\Mongodb\Auth\User as Authenticatable;

class User extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract {

    use Authenticatable, Authorizable;

    protected $collection = 'CSUsers';

    protected $guarded = ['_id'];
    protected $guard = 'api';
    protected $guard_name = 'api';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'phone',
        'active'
    ];
    

    protected $hidden = [
        'password',
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'auth_token' => $this->getAuthToken()
        ];
    }

    public function setAuthToken($token){
        $this->auth_token = $token;
    }

    public function getAuthToken(){
        return $this->auth_token;
    }
    
}