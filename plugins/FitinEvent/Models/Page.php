<?php

namespace Plugin\FitinEvent\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

class Page extends Eloquent {

    const DRIVER = 'public';
    
    protected $collection = 'EventPages';
    
    protected $fillable = [
        "code",
        "title",
        "content",
        "media"
    ];

    public function meta(){
        return $this->hasMany(PageMeta::class, 'code', 'code');
    }

    public function getMediaUrlAttribute(){
        $driver = self::DRIVER;
        $url = ($this->media) ? Storage::disk($driver)->url($this->media) : null;
        return $url;
    }
    
}