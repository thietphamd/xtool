<?php

namespace Plugin\FitinEvent\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

class Subscriber extends Eloquent {

    const DRIVER = 'public';
    
    protected $collection = 'EventSubscribers';
    
    protected $fillable = [
        "name",
        "email",
        "meta_id"
    ];

    public function page_meta(){
        return $this->belongsToMany(PageMeta::class);
    }
    
}