<?php

namespace Plugin\FitinEvent\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

class PageMeta extends Eloquent {

    const DRIVER = 'public';
    
    protected $collection = 'EventPageMetas';
    
    protected $fillable = [
        "code",
        "title",
        "content",
        "media"
    ];

    public function page(){
        return $this->belongsTo(Page::class, 'code', 'code');
    }

    public function subscribers(){
        return $this->belongsToMany(Subscriber::class);
    }


    public function getMediaUrlAttribute(){
        $driver = self::DRIVER;
        $url = ($this->media) ? Storage::disk($driver)->url($this->media) : null;
        return $url;
    }
    
}