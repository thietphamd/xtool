<?php

namespace Plugin\FitinEvent\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PageResourceCollection extends ResourceCollection
{
    public $collects = 'Plugin\FitinEvent\Http\Resources\PageResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
                  
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,     
            'message' => 'Success', 
        ];
    }
}
