<?php

namespace Plugin\FitinEvent\Http\Controllers;

use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Plugin\FitinEvent\Models\Subscriber;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


use Plugin\FitinEvent\Http\Resources\SubscriberResource;

use Plugin\FitinEvent\Repositories\SubscriberRepository;

class SubscriberController extends Controller
{

    protected $repository;

    public function __construct(SubscriberRepository $repository){
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'email'   => 'required'
    
        ]);
        
        if($validator->fails()){
            return response()->json([
                'code' => -1,
                'message' => 'Email required'
            ]);
        }
        $subscriber = $this->repository->updateOrCreate($input);

        return new SubscriberResource($subscriber);
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        $subscriber = $this->repository->find($key);

        if(!$subscriber){
            return response()->json([
                'code' => -1,
                'message' => 'Subscriber not found!'
            ]);
        }
        return new SubscriberResource($subscriber);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param 
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param 
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        
    }


    public function subscribe(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'email'   => 'required',
            'meta_id' => 'required'
    
        ]);
        
        if($validator->fails()){
            return response()->json([
                'code' => -1,
                'message' => 'Email, meta_id required'
            ]);
        }

        $sub = $this->repository->subscribe($input);

        if(!$sub){
            return response()->json([
                'code' => -1,
                'message' => 'Data not found'
            ]);
        }

        return new SubscriberResource($sub);

    }
    
}
