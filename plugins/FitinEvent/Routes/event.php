<?php
Route::group([
    'prefix' => 'v1',
], function(){
    Route::post('page/meta', 'PageController@storeMeta')->middleware('private-token'); 
    Route::put('page/meta/{id}', 'PageController@updateMeta')->middleware('private-token'); 
    Route::delete('page/meta/{key}', 'PageController@destroyMeta')->middleware('private-token'); 

    Route::get('page/{key}', 'PageController@show');
    Route::post('page', 'PageController@store')->middleware('private-token'); 
    Route::put('page/{key}', 'PageController@update')->middleware('private-token'); 
    Route::delete('page/{key}', 'PageController@destroy')->middleware('private-token'); 

    Route::get('subscriber/{key}', 'SubscriberController@show');
    Route::post('subscriber', 'SubscriberController@store')->middleware('private-token'); 
    //Route::put('subscriber/{key}', 'SubscriberController@update')->middleware('private-token'); 
    //Route::delete('subscriber/{key}', 'SubscriberController@destroy')->middleware('private-token'); 
    Route::post('subscriber/meta', 'SubscriberController@subscribe')->middleware('private-token'); 
});


 