<?php
namespace Plugin\FitinEvent\Repositories;

use App\Repositories\EloquentRepository;
use Plugin\FitinEvent\Models\Page;
use Plugin\FitinEvent\Models\PageMeta;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PageRepository extends EloquentRepository implements PageRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    public function __construct()
    {
        $this->model = app($this->model());
       
    }

	public function model()
	{
		return Page::class;
	}
    
    public function getAll()
    {
        return $this->model->all();
    }

    
    
	public function create($input)
	{
		$page = $this->model->create($input);
        return $page;
    }

    public function createMeta($input)
	{
		$page_meta = PageMeta::create($input);
        return $page_meta;
    }

    public function uploadMedia($page, $file){
        $driver = Page::DRIVER;
        $arrFileName = pathinfo($file->getClientOriginalName());
        $file_base_name = strtolower($arrFileName['basename']);
        $time = \Carbon\Carbon::now()->timestamp;   
        $path = "/pages/{$page->code}/$time";
        $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
        $page->media = $upload;
        $page->save();
        return $page;
    }

    public function uploadMetaMedia($page_meta, $file){
        $driver = PageMeta::DRIVER;
        $arrFileName = pathinfo($file->getClientOriginalName());
        $file_base_name = strtolower($arrFileName['basename']);
        $time = \Carbon\Carbon::now()->timestamp;   
        $path = "/pages/{$page_meta->page->code}/{$page_meta->_id}/$time";
        $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
        $page_meta->media = $upload;
        $page_meta->save();
        return $page_meta;
    }

    public function find($key){
        return $this->model->where('_id', $key)->orWhere('code', $key)->first(); 
    }


    public function updateMeta($key, $input){
        $page_meta = PageMeta::where('_id', $key)->first();
        if(!$page_meta){
            return false;
        }
        $page_meta->update($input);

        return $page_meta;
    }

    public function delete($key){
        $page = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        
        if(!$page){
            return false;
        }
        $page->delete();
        $page->meta()->delete();
        return $page;
    }

    public function deleteMeta($key){
        $page_meta = PageMeta::where('_id', $key)->first();
        
        if(!$page_meta){
            return false;
        }
        $page_meta->delete();
        return $page_meta;
    }

}