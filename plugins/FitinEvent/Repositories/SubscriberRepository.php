<?php
namespace Plugin\FitinEvent\Repositories;

use App\Repositories\EloquentRepository;
use Plugin\FitinEvent\Models\Subscriber;
use Plugin\FitinEvent\Models\PageMeta;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class SubscriberRepository extends EloquentRepository implements SubscriberRepositoryInterface {
    
    const CACHE_TIME = 1;

    public function __construct()
    {
        $this->model = app($this->model());
       
    }

	public function model()
	{
		return Subscriber::class;
	}
    
    public function getAll()
    {
        return $this->model->all();
    }

    
    
	public function create($input)
	{
		$subscriber = $this->model->create($input);
        return $subscriber;
    }

    public function updateOrCreate($input)
	{
		$subscriber = $this->model->updateOrCreate(['email' => $input['email']], $input);
        return $subscriber;
    }


    public function find($key){
        return $this->model->where('_id', $key)->orWhere('email', $key)->first(); 
    }


    public function subscribe($input){
        $email = $input['email'];
        $meta_id = $input['meta_id'];
        $subscriber = $this->find($email);
        $meta = PageMeta::find($meta_id);
        if(!$subscriber || !$meta){
            return false;
        }
        $subscribe_meta = $subscriber->page_meta()->save($meta);
        return $subscriber;
    }

}