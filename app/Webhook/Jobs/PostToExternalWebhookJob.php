<?php

namespace App\Webhook\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use App\Webhook\Models\UserWebhook;

class PostToExternalWebhookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
        * Create a new job instance.
        *
        * @return void
        */
    public $tries = 5;

    protected $action, $data, $webhook_url, $type;

    public function __construct($webhook_url, $type, $action, $data)
    {
        //
        $this->action = $action;
        $this->data = $data;
        $this->type = $type;
        $this->webhook_url = $webhook_url;
    }

    /**
        * Execute the job.
        *
        * @return void
        */
    public function handle()
    {
        $action = $this->action;
        $data = $this->data;
        $webhook_url = $this->webhook_url;
        $type = $this->type;

        $method = explode('.', $action)[1];
        
        $postData = [
            'type' => $type,
            'action' => $method,
            'data' => $data
        ];
        
        try {
            $arrData = [
                'timeout' => 10, // Response timeout
                'connect_timeout' => 10, // Connection timeout
                'headers' => [
                    'Access-Key' => config('fitin.partner_token'),
                    'User-Agent' => config('fitin.static_user_agent')         
                ],
                'json' => $postData 
            ];
            
            $client = new Client();
            $response = $client->request('POST',  $webhook_url , $arrData);
            if ($response->getStatusCode() == 200) {
                $jsonResult = $response->getBody()->getContents();
                $result = json_decode($jsonResult, 1);
                
                Log::channel('webhook')->debug($result);
            }
            Log::channel('webhook')->debug($postData);
            UserWebhook::where('webhook', $webhook_url)->update(['failed' => 0]);
        }catch(RequestException $e){
            UserWebhook::where('webhook', $webhook_url)->increment('failed', 1);
            //UserWebhook::where('webhook', $webhook_url)->where('failed', '>', 4)->update(['active' => false]);
        
            $arrError = [
                'api' => config('app.url'),
                'error' => class_basename($e) . ' | ' .$e->getMessage(),
                'stack' => mb_substr($e->getTraceAsString(), 0, 150)  
            ];
            $channel = env('LOG_CHANNEL', 'telegram');
            Log::channel($channel)->debug($arrError);

        }catch(\Exception $e){
            $arrError = [
                'api' => config('app.url'),
                'error' => class_basename($e) . ' | ' .$e->getMessage(),
                'stack' => mb_substr($e->getTraceAsString(), 0, 150)  
            ];
            $channel = env('LOG_CHANNEL', 'telegram');
            Log::channel($channel)->debug($arrError);
        }
    }
}
