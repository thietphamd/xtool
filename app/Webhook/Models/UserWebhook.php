<?php

namespace App\Webhook\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserWebhook extends Eloquent {

    public static function boot()
    {
        parent::boot();   

        self::deleted(function ($model) {
            $model->resources()->delete();
        });
    }

    protected $collection = 'UserWebhooks';

    protected $fillable = [
        "userId" ,
        "webhook",
        "active",
        "failed"
       
    ];

    public const CACHE_TAG = 'user-webhooks';

    public function resources(){
        return $this->hasMany(UserWebhookResource::class, 'userId', 'userId');
    }
}