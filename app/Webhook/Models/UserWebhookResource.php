<?php

namespace App\Webhook\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\CacheTrait;

class UserWebhookResource extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'UserWebhookResources';

    protected $fillable = [
        "userId" ,
        "key",
        "model",
        "type",
        "action"
       
    ];

    public const CACHE_TAG = 'user-webhook-resources';

    public function user(){
        return $this->belongsTo(UserWebhook::class, 'userId', 'userId');
    }
}