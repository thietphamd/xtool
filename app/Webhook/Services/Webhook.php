<?php
namespace App\Webhook\Services;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Webhook\Jobs\PostToExternalWebhookJob;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
class Webhook
{
    protected $url, $type, $action, $data;

    public function __construct($url, $type, $action , $data){
        $this->action = $action;
        $this->data = $data;
        $this->url = $url;
        $this->type = $type;
    }

    public function post(){     
        PostToExternalWebhookJob::dispatch( $this->url, $this->type, $this->action, $this->data)->onQueue('3dresource');       
    }
}
