<?php

Route::post('ecom/callback', 'EcomController@callback');

Route::group([
    //'namespace' => 'Webhook',
    // 'middleware' => 'auth:token'
], function () {
    Route::post('ecom/callback', 'EcomController@callback');

    Route::get('model/action', 'WebhookController@getModelAction')->middleware('private-token'); 

    Route::post('user/subscribe', 'WebhookResourceController@createFullAction')->middleware('private-token');

    Route::post('user/action', 'WebhookController@createAction')->middleware('private-token'); 
    Route::get('user/action', 'WebhookController@indexAction')->middleware('private-token'); 
    
    Route::post('user', 'WebhookController@store')->middleware('private-token'); 
    Route::put('user', 'WebhookController@update')->middleware('private-token'); 
    Route::delete('user', 'WebhookController@destroy')->middleware('private-token'); 
});
