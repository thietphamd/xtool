<?php

namespace App\Webhook\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use App\Models\Activity\ModelAction;
use App\Webhook\Models\UserWebhook;

use App\Http\Resources\Activity\ModelActionResource;
use App\Http\Resources\Activity\ModelActionResourceCollection;
use App\Webhook\Http\Resources\UserWebhookResource;
use App\Webhook\Http\Resources\UserWebhookResourceCollection;

use App\Webhook\Repositories\WebhookRepository;
use App\Http\Controllers\Api_V2\ApiController;

class WebhookController extends ApiController
{
    protected $webhookRepository;

    public function __construct(WebhookRepository $webhookRepository){
        $this->webhookRepository = $webhookRepository;
    }

    public function getModelAction(Request $request){
        $methods = $this->webhookRepository->getModelActions();
        return new ModelActionResourceCollection($methods);
    }

    public function index(Request $request){
        $webhooks = $this->webhookRepository->getAll();
        return new UserWebhookResourceCollection($webhooks);
    }

    public function store(Request $request){
        $input = $request->all();
        $validator = Validator::make($input, [
            'webhook'   => 'required',
        ]);
        if($validator->fails()){
            return $this->responseError('Webhook required'); 
        }
        $webhook = $this->webhookRepository->create($input);
        return new UserWebhookResource($webhook);
    }

    public function update(Request $request){
        $input = $request->only(['webhook', 'userId']);
        $webhook = $this->webhookRepository->update($userId);
        if(!$webhook){
            return $this->responseError('Webhook not found');
        }
        return new UserWebhookResource($webhook);
    }

    public function destroy(Request $request){
        $userId = $request->get('userId');
        $webhook = $this->webhookRepository->delete($userId);

        if(!$webhook){
            return $this->responseError('Webhook not found or already deleted before');
        }

        return response()->json([
            'code' => 0,
            'message' => "Success"
        ]);
    }


}
