<?php

namespace App\Webhook\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs\SyncFromWebhookJob;

class EcomController extends Controller
{

    protected function callback(Request $request)
    {
        return response()->json(
            [
                "code" => 200,
                "message" => "Temporary disable this feature!"
            ]
        );

        $type = $request->get('type');
        $action = $request->get('action');
        $data = $request->get('data');
        
        if(!$type || !$action || !$data){
            return response()->json(
                [
                    "code" => 400,
                    "error" => 1,
                    "message" => "Missing information. 'type', 'action', 'data' required "
                ]
            );
        }
        //TO DO
        if(config('fitin.enabled_sync_from_external')){
            SyncFromWebhookJob::dispatch($request->all())->onQueue('fitin3d');
        }
        

        return response()->json(
            [
                "code" => 200,
                "message" => "success"
            ]
        );
    }

    
}
