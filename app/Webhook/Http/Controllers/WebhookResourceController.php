<?php

namespace App\Webhook\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use App\Models\Activity\ModelAction;
use App\Webhook\Models\UserWebhookResource;

use App\Webhook\Http\Resources\UserWebhookResResource;
use App\Webhook\Http\Resources\UserWebhookResResourceCollection;

use App\Webhook\Repositories\WebhookResourceRepository;
use App\Http\Controllers\Api_V2\ApiController;

class WebhookResourceController extends ApiController
{
    protected $webhookResRepository;

    public function __construct(WebhookResourceRepository $webhookResRepository){
        $this->webhookResRepository = $webhookResRepository;
    }


    public function index(Request $request){
        $webhooks = $this->webhookResRepository->getAll();
        return new UserWebhookResResourceCollection($webhooks);
    }

    public function store(Request $request){
    
    }

    public function update(Request $request, $id){
        
    }

    public function destroy(Request $request, $id){
        
    }


    /**
     * FOR USER ACTION
     */
    public function createFullAction(Request $request){
        $input = $request->only(['type', 'key', 'userId']);
        $validator = Validator::make($input, [
            'type'   => 'required',
            'userId'  => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('type or header token required'); 
        }

        $type = $input['type'];
        $method = ModelAction::where('type', $type)->first();
        if(!$method){
            return $this->responseError("$type is not supported."); 
        }

        $action = $this->webhookResRepository->createFull($input);
        if(!$action){
            return $this->responseError("Data Not found."); 
        }

        return $this->respondSuccess(); 
    }    
}
