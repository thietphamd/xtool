<?php

namespace App\Webhook\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserWebhookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'userId' => $this->userId,
            'webhook' => $this->webhook,
            'active' => $this->active
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
