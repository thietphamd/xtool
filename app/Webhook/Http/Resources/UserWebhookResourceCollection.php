<?php

namespace App\Webhook\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserWebhookResourceCollection extends ResourceCollection
{
    public $collects = 'App\Webhook\Http\Resources\UserWebhookResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,     
            'message' => 'Success', 
        ];
    }
}
