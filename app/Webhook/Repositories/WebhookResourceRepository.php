<?php
namespace App\Webhook\Repositories;

use App\Repositories\EloquentRepository;
use App\Models\Activity\ModelAction;
use App\Webhook\Models\UserWebhookResource;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class WebhookResourceRepository extends EloquentRepository implements WebhookResourceRepositoryInterface {
    
    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return UserWebhookResource::class;
    }
    
    public function getAll($params = [])
    {
        
    }

    
	public function create($input)
	{
       
    }
    
    public function find($key){
        
    }


    public function update($key, $input){
        
    }

    public function delete($key){
        
    }

    public function createFull($input){
       
        if(!empty($input['key'])){
            return $this->subscribeSpecificItem($input);
        }
        else{
            return $this->subscribeFull($input);
        }
    }

    protected function subscribeSpecificItem($input){
        $type = $input['type'];
        $key = $input['key'];
        
        $model_actions = ModelAction::where('type', $type)->where('param', true)->get();
        $model = $model_actions->first()->model;
        $instance = $model::find($key);
        if(!$instance){
            return false;
        }
        
        foreach($model_actions as $action){           
            $data = array_merge($input, [
                'action' => $action->action,
                'userId' => $input['userId'],
                'model' => $action->model
            ]);
            $this->model->firstOrCreate($data, $data);
        }
        
        return true;
    }

    protected function subscribeFull($input){
        $type = $input['type'];
        $model_actions = ModelAction::where('type', $type)->where('param', false)->get();
        $model = $model_actions->first()->model;
        foreach($model_actions as $action){           
            $data = array_merge($input, [
                'action' => $action->action,
                'userId' => $input['userId'],
                'model' => $action->model
            ]);
            $this->model->firstOrCreate($data, $data);
        }
        
        return true;
    }
}