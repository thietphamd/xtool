<?php


namespace App\Webhook\Repositories;

use App\Repositories\RepositoryInterface;

interface WebhookResourceRepositoryInterface extends RepositoryInterface {

}