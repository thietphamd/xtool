<?php
namespace App\Webhook\Repositories;

use App\Repositories\EloquentRepository;
use App\Models\Activity\ModelAction;
use App\Webhook\Models\UserWebhook;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class WebhookRepository extends EloquentRepository implements WebhookRepositoryInterface {
    
    public function __construct()
    {
        
    }

    public function model()
    {
        
    }
    
    public function getAll($params = [])
    {
        $userId = (int) request()->get('userId');
        $query = new UserWebhook;
        if($userId){
            $query = $query->where('userId', $userId);
        }
        return $query->get();
    }

    public function getModelActions()
    {
        $methods = ModelAction::all();
        return $methods;
    }

    
    
	public function create($input)
	{
        $input['active'] = true;
        $userWebhook = UserWebhook::firstOrCreate(['userId' => $input['userId']], $input);
        return $userWebhook;
    }
    
    public function find($key){
        
    }


    public function update($key, $input){
        $userWebhook = UserWebhook::find($key);
        if(!$userWebhook){
            return false;
        }
        $userWebhook->update($input);
        return $userWebhook;
    }

    public function delete($userId){
        $userWebhook = UserWebhook::where('userId', $userId)->first();
        if(!$userWebhook){
            return false;
        }
        $userWebhook->delete();
        return true;
    }
}