<?php
namespace App\Repositories;

use App\Models\TenantUser;
use App\Repositories\EloquentRepository;

class TenantUserRepository extends EloquentRepository implements TenantUserRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return TenantUser::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $active = $params['active'] ?? 0;
        $query = $this->model;
        if ($name) {

            $query = $query->where(function ($q) use ($name) {
                $q->where('name', 'like', '%' . $name . '%')
                    ->orWhere('email', 'like', '%' . $name . '%');
            });
        }

        if ($active) {
            $query = $query->active();
        }

        if ($limit) {
            return $query->latest()->paginate($limit);
        } else {
            return $query->latest()->get();
        }
    }

    public function create($input)
    {
        if (!isset($input['active'])) {
            $input['active'] = TenantUser::ACTIVE;
        } else if ($input['active']) {
            $input['active'] = TenantUser::ACTIVE;
        } else {
            $input['active'] = TenantUser::INACTIVE;
        }

        $tenant = $this->model->create($input);

        return $tenant;
    }

    public function find($key)
    {
        $hashkey = \RedisCache::buildHash($this->model);
        $data = \RedisCache::hget($hashkey, $key);
        if ($data) {
            return $data;
        }

        $data = $this->model->find($id);
        \RedisCache::hset($hashkey, $key, $data);
        return $data;
    }

    public function update($key, $input)
    {
        if (!isset($input['active'])) {
            $input['active'] = TenantUser::ACTIVE;
        } else if ($input['active']) {
            $input['active'] = TenantUser::ACTIVE;
        } else {
            $input['active'] = TenantUser::INACTIVE;
        }
        $tenant = $this->model->where('_id', $key)->orWhere('auth_id', (int) $key)->first();
        if (!$tenant) {
            return false;
        }
        $tenant->update($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $tenant->_id, $tenant);
        \RedisCache::hset($hashkey, $tenant->auth_id, $tenant);
        return $tenant;
    }

    public function delete($key)
    {
        $tenant = $this->model->where('_id', $key)->orWhere('auth_id', (int) $key)->first();
        if (!$tenant) {
            return false;
        }

        $tenant->delete();
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hdel($hashkey, $tenant->auth_id);
        \RedisCache::hdel($hashkey, $tenant->_id);
        return $tenant;
    }
}
