<?php
namespace App\Repositories;

use App\Models\AssetStyleFit;
use App\Repositories\EloquentRepository;

class AssetStyleFitRepository extends EloquentRepository implements AssetStyleFitRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());

    }

    public function model()
    {
        return AssetStyleFit::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $brand_id = $params['brand_id'] ?? null;
        $category_id = $params['category_id'] ?? null;

        $query = $this->model;
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        if ($category_id) {
            $query = $query->where('category_id', $category_id);
        }

        if ($brand_id) {
            $query = $query->where('brand_id', $brand_id);
        }

        if ($limit) {
            return $query->latest()->orderBy('_id', 'DESC')->paginate($limit);
        } else {
            return $query->latest()->orderBy('_id', 'DESC')->get();
        }
    }

    public function create($input)
    {
        if (empty($input['status'])) {
            $input['status'] = AssetStyleFit::ACTIVE;
        }

        if (empty($input['attributes'])) {
            $object_attributes = AssetStyleFit::DEFAULT_ASSET_ATTRIBUTES;

            $input['attributes'] = $object_attributes;

        }
        $assetStyleFit = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $assetStyleFit->_id, $assetStyleFit);
        return $assetStyleFit;
    }

}
