<?php
namespace App\Repositories;

use App\Models\SubscriptionPlan;
use App\Repositories\EloquentRepository;

class SubscriptionPlanRepository extends EloquentRepository implements SubscriptionPlanRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return SubscriptionPlan::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $query = $this->model;
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        if ($limit) {
            return $query->latest()->paginate($limit);
        } else {
            return $query->latest()->get();
        }
    }

    public function create($input)
    {
        if (empty($input['status'])) {
            $input['status'] = SubscriptionPlan::PENDING;
        }
        $subscriptionPlan = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $subscriptionPlan->_id, $subscriptionPlan);
        return $subscriptionPlan;
    }

    public function addOption($key, $input)
    {
        $subscriptionPlan = $this->model->where('_id', $key)->first();
        if (!$subscriptionPlan) {
            return false;
        }
        $subscriptionPlan->options()->create($input);
        $subscriptionPlan->touch();

        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $subscriptionPlan->_id, $subscriptionPlan);

        return $subscriptionPlan;
    }

    public function updateOption($key, $option_id, $input)
    {
        $subscriptionPlan = $this->model->where('_id', $key)->first();

        if (!$subscriptionPlan) {
            return false;
        }
        $option = $subscriptionPlan->options()->where('_id', $option_id)->first();
        $option->update($input);
        $subscriptionPlan->touch();

        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $subscriptionPlan->_id, $subscriptionPlan);

        return $subscriptionPlan;
    }

    public function removeOption($key, $option_id)
    {
        $subscriptionPlan = $this->model->where('_id', $key)->first();
        if (!$subscriptionPlan) {
            return false;
        }
        $option = $subscriptionPlan->options()->where('_id', $option_id)->first();
        if ($option) {
            $subscriptionPlan->options()->destroy($option);
        }
        $subscriptionPlan->touch();

        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $subscriptionPlan->_id, $subscriptionPlan);
        return $subscriptionPlan;
    }

}
