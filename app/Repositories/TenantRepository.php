<?php
namespace App\Repositories;

use App\Models\Tenant;
use App\Repositories\EloquentRepository;

class TenantRepository extends EloquentRepository implements TenantRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return Tenant::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $active = $params['active'] ?? 0;
        $query = $this->model;
        if ($name) {

            $query = $query->where(function ($q) use ($name) {
                $q->where('name', 'like', '%' . $name . '%')
                    ->orWhere('email', 'like', '%' . $name . '%');
            });
        }

        if ($active) {
            $query = $query->active();
        }

        if ($limit) {
            return $query->latest()->paginate($limit);
        } else {
            return $query->latest()->get();
        }
    }

    public function create($input)
    {
        if (!isset($input['active'])) {
            $input['active'] = Tenant::ACTIVE;
        }
        $tenant = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $tenant->_id, $tenant);
        return $tenant;
    }

}
