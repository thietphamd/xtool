<?php
namespace App\Repositories;

use App\Models\Texture;
use App\Repositories\EloquentRepository;

class TextureRepository extends EloquentRepository implements TextureRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return Texture::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $group_id = $params['group_id'] ?? null;
        $query = $this->model;
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        if ($group_id) {
            $query = $query->where('group_id', $group_id);
        }
        if ($limit) {
            return $query->paginate($limit);
        } else {
            return $query->get();
        }
    }

}
