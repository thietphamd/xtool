<?php
namespace App\Repositories;

use App\Models\SystemConfig;
use App\Repositories\EloquentRepository;

class SystemConfigRepository extends EloquentRepository implements SystemConfigRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return SystemConfig::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $active = $params['active'] ?? 0;
        $query = $this->model;
        if ($name) {

            $query = $query->where(function ($q) use ($name) {
                $q->where('name', 'like', '%' . $name . '%')
                    ->orWhere('email', 'like', '%' . $name . '%');
            });
        }

        if ($active) {
            $query = $query->active();
        }

        if ($limit) {
            return $query->paginate($limit);
        } else {
            return $query->get();
        }
    }

    public function create($input)
    {
        if (!isset($input['status'])) {
            $input['status'] = SystemConfig::ACTIVE;
        }
        $systemConfig = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $systemConfig->_id, $systemConfig);
        return $systemConfig;
    }

}
