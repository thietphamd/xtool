<?php
namespace App\Repositories;

use App\Repositories\EloquentRepository;
use App\Models\Plan;
use App\Models\SystemConfig;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class PlanRepository extends EloquentRepository implements PlanRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Plan::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $status = $params['status'] ?? null;
        $builder_id = $params['builder_id'] ?? null;
        $query = $this->model;
        if($name){
            $query = $query->where('name', 'like', '%'. $name . '%');
        }

        if($status){
            $query = $query->where('status', strtolower($status));
        }

        if($builder_id){
            $query = $query->where('builder_id', $builder);
        }
        
        if($limit){
            return $query->latest()->paginate($limit);
        }
        else {
            return $query->latest()->get();
        }
    }

    
	public function create($input)
	{
        if(empty( $input['status'])){
            $input['status'] = Plan::ACTIVE;
        }
        
        if(empty( $input['metadata'])){
            $input['metadata'] = Plan::DEFAULT_METADATA;
        }
        

		$plan = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $plan->_id, $plan);
        return $plan;
    }
    


    public function update($key, $input){
        $plan = $this->model->find($key);
        if(!$plan){
            return false;
        }

        $classes = SystemConfig::getResourceClasses();
        if(empty($input['class'])){
            $input['class'] = $classes;
        }else{
            $merged_keyed = array_column(array_merge($classes, $input['class']), NULL, 'code');
            $input['class'] = (array_values($merged_keyed));
        }

        $plan->update($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $key, $plan);
        return $plan;
    }

}