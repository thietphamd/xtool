<?php
namespace App\Repositories;

use App\Models\Payment;
use App\Repositories\EloquentRepository;

class PaymentRepository extends EloquentRepository implements PaymentRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return Payment::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $tenant = $params['tenant'] ?? null;
        $subscription = $params['subscription'] ?? null;
        $query = $this->model;
        if ($tenant) {
            $query = $query->where('tenant_id', $tenant);
        }

        if ($subscription) {
            $query = $query->where('subscription_plan_id', $subscription);
        }

        if ($limit) {
            return $query->latest()->paginate($limit);
        } else {
            return $query->latest()->get();
        }
    }

    public function create($input)
    {
        if (empty($input['status'])) {
            $input['status'] = Payment::PENDING;
        }
        $payment = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $payment->_id, $payment);
        return $payment;
    }

}
