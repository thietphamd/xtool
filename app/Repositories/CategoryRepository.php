<?php
namespace App\Repositories;

use App\Models\Category;
use App\Models\SystemConfig;
use App\Repositories\EloquentRepository;

class CategoryRepository extends EloquentRepository implements CategoryRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return Category::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $status = $params['status'] ?? null;
        $class = $params['class'] ?? null;
        $query = $this->model;
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        if ($status) {
            $query = $query->where('status', strtolower($status));
        }

        if ($class) {
            $query = $query->where('class', ['$elemMatch' => ['code' => $class, 'value' => 1]]);
        }

        if ($limit) {
            return $query->paginate($limit);
        } else {
            return $query->get();
        }
    }

    public function create($input)
    {
        if (empty($input['status'])) {
            $input['status'] = Category::ACTIVE;
        }
        $classes = SystemConfig::getResourceClasses();
        if (empty($input['class'])) {
            $input['class'] = $classes;
        } else {
            $merged_keyed = array_column(array_merge($classes, $input['class']), null, 'code');
            $input['class'] = (array_values($merged_keyed));
        }

        $category = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $category->_id, $category);
        return $category;
    }

    public function update($key, $input)
    {
        $category = $this->model->find($key);
        if (!$category) {
            return false;
        }

        $classes = SystemConfig::getResourceClasses();
        if (empty($input['class'])) {
            $input['class'] = $classes;
        } else {
            $merged_keyed = array_column(array_merge($classes, $input['class']), null, 'code');
            $input['class'] = (array_values($merged_keyed));
        }

        $category->update($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $key, $category);
        return $category;
    }

}
