<?php
namespace App\Repositories;

use App\Models\SubscriptionApplication;
use App\Repositories\EloquentRepository;

class SubscriptionApplicationRepository extends EloquentRepository implements SubscriptionApplicationRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return SubscriptionApplication::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $query = $this->model;
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        if ($limit) {
            return $query->latest()->paginate($limit);
        } else {
            return $query->latest()->get();
        }
    }

    public function create($input)
    {

        $subscriptionApplication = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $subscriptionApplication->_id, $subscriptionApplication);
        return $subscriptionApplication;
    }

}
