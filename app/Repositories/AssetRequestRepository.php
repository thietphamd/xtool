<?php
namespace App\Repositories;

use App\Models\AssetRequest;
use App\Repositories\EloquentRepository;

class AssetRequestRepository extends EloquentRepository implements AssetRequestRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());

    }

    public function model()
    {
        return AssetRequest::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $model = $params['model'] ?? null;
        $tenant_id = $params['tenant_id'] ?? null;
        $status = $params['status'] ?? null;
        $query = $this->model;
        if ($model) {
            switch ($model) {
                case 'material':
                    $query = $query->where('model', 'App\\Models\\Material');
                    break;
                case 'asset':
                    $query = $query->where('model', 'App\\Models\\Asset');
                    break;
                case 'stylefit':
                    $query = $query->where('model', 'App\\Models\\AAssetStyleFit');
                    break;
                default;
                    break;
            };

        }

        if ($tenant_id) {
            $query = $query->where('tenant_id', $tenant_id);
        }

        if ($status) {
            $query = $query->where('status', $status);
        }

        if ($limit) {
            return $query->latest()->orderBy('_id', 'DESC')->paginate($limit);
        } else {
            return $query->latest()->orderBy('_id', 'DESC')->get();
        }
    }

}
