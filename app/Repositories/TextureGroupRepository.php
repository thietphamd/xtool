<?php
namespace App\Repositories;

use App\Models\TextureGroup;
use App\Repositories\EloquentRepository;

class TextureGroupRepository extends EloquentRepository implements TextureGroupRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return TextureGroup::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;

        $query = $this->model;
        $query = $query->whereNull('parent_id');
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        if ($limit) {
            return $query->paginate($limit);
        } else {
            return $query->get();
        }

    }

}
