<?php
namespace App\Repositories;

use App\Models\Builder;
use App\Models\SystemConfig;
use App\Repositories\EloquentRepository;

class BuilderRepository extends EloquentRepository implements BuilderRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return Builder::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $status = $params['status'] ?? null;

        $query = $this->model;
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        if ($status) {
            $query = $query->where('status', strtolower($status));
        }

        if ($limit) {
            return $query->paginate($limit);
        } else {
            return $query->get();
        }
    }

    public function create($input)
    {
        if (empty($input['status'])) {
            $input['status'] = Builder::ACTIVE;
        }
        $classes = SystemConfig::getResourceClasses();
        if (empty($input['class'])) {
            $input['class'] = $classes;
        } else {
            $merged_keyed = array_column(array_merge($classes, $input['class']), null, 'code');
            $input['class'] = (array_values($merged_keyed));
        }

        $builder = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $builder->_id, $builder);
        return $builder;
    }

    public function update($key, $input)
    {
        $builder = $this->model->find($key);
        if (!$builder) {
            return false;
        }

        $classes = SystemConfig::getResourceClasses();
        if (empty($input['class'])) {
            $input['class'] = $classes;
        } else {
            $merged_keyed = array_column(array_merge($classes, $input['class']), null, 'code');
            $input['class'] = (array_values($merged_keyed));
        }

        $builder->update($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $key, $builder);
        return $builder;
    }

}
