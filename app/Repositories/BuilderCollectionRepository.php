<?php
namespace App\Repositories;

use App\Models\BuilderCollection;
use App\Repositories\EloquentRepository;

class BuilderCollectionRepository extends EloquentRepository implements BuilderCollectionRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return BuilderCollection::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $status = $params['status'] ?? null;

        $query = $this->model;
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        if ($status) {
            $query = $query->where('status', strtolower($status));
        }

        if ($limit) {
            return $query->paginate($limit);
        } else {
            return $query->get();
        }
    }

    public function create($input)
    {
        if (empty($input['status'])) {
            $input['status'] = BuilderCollection::ACTIVE;
        }

        $builderCollection = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $builderCollection->_id, $builderCollection);
        return $builderCollection;
    }

}
