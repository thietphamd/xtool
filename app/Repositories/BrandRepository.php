<?php
namespace App\Repositories;

use App\Models\Brand;
use App\Models\SystemConfig;
use App\Repositories\EloquentRepository;

class BrandRepository extends EloquentRepository implements BrandRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return Brand::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $name = $params['name'] ?? null;
        $status = $params['status'] ?? null;
        $class = $params['class'] ?? null;
        $query = $this->model;
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        if ($status) {
            $query = $query->where('status', strtolower($status));
        }

        if ($class) {
            $query = $query->where('class', ['$elemMatch' => ['code' => $class, 'value' => 1]]);
        }

        if ($limit) {
            return $query->paginate($limit);
        } else {
            return $query->get();
        }
    }

    public function create($input)
    {
        if (empty($input['status'])) {
            $input['status'] = Brand::ACTIVE;
        }
        $classes = SystemConfig::getResourceClasses();
        if (empty($input['class'])) {
            $input['class'] = $classes;
        } else {
            $merged_keyed = array_column(array_merge($classes, $input['class']), null, 'code');
            $input['class'] = (array_values($merged_keyed));
        }

        $brand = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $brand->_id, $brand);
        return $brand;
    }

    public function update($key, $input)
    {
        $brand = $this->model->find($key);
        if (!$brand) {
            return false;
        }

        $classes = SystemConfig::getResourceClasses();
        if (empty($input['class'])) {
            $input['class'] = $classes;
        } else {
            $merged_keyed = array_column(array_merge($classes, $input['class']), null, 'code');
            $input['class'] = (array_values($merged_keyed));
        }

        $brand->update($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $key, $brand);
        return $brand;
    }

}
