<?php
namespace App\Repositories;

use App\Models\Invoice;
use App\Repositories\EloquentRepository;

class InvoiceRepository extends EloquentRepository implements InvoiceRepositoryInterface
{

    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function model()
    {
        return Invoice::class;
    }

    public function getAll($params = [])
    {
        $limit = (int) ($params['limit'] ?? 0);
        $page = (int) ($params['page'] ?? 0);
        $tenant = $params['tenant'] ?? null;
        $subscription = $params['subscription'] ?? null;
        $query = $this->model;
        if ($tenant) {
            $query = $query->where('tenant_id', $tenant);
        }

        if ($subscription) {
            $query = $query->where('subscription_plan_id', $subscription);
        }

        if ($limit) {
            return $query->latest()->paginate($limit);
        } else {
            return $query->latest()->get();
        }
    }

    public function create($input)
    {

        $invoice = $this->model->create($input);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $invoice->_id, $invoice);
        return $invoice;
    }

}
