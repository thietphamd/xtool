<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;

abstract class EloquentRepository implements RepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * EloquentRepository constructor.
     */
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * get model
     * @return string
     */
    abstract public function model();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->model()
        );
    }

    /**
     * Get All
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll($params)
    {
        return $this->model->all();
    }

    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $hashkey = \RedisCache::buildHash($this->model);
        $data = \RedisCache::hget($hashkey, $id);
        if ($data) {
            return $data;
        }

        $data = $this->model->find($id);
        \RedisCache::hset($hashkey, $id, $data);
        return $data;
    }

    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create($attributes)
    {
        $data = $this->model->create($attributes);
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $data->_id, $data);
        return $data;
    }

    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return bool|mixed
     */
    public function update($key, $attributes)
    {
        $data = $this->model->find($key);
        if (!$data) {
            return false;
        }

        $data->update($attributes);

        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hset($hashkey, $key, $data);

        return $data;
    }

    /**
     * Delete
     *
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $data = $this->model->find($id);
        if (!$data) {
            return false;
        }

        $data->delete();
        $hashkey = \RedisCache::buildHash($this->model);
        \RedisCache::hdel($hashkey, $id);
        return $data;
    }

    /**
     * paginate
     *
     * @param $perPage, $column
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = array('*'), $method = "paginate")
    {
        /* $this->newQuery()->eagerLoadRelations();
        $this->applyScope(); */
        return $this->model->orderBy('id', 'DESC')->{$method}($perPage, $columns);
    }
}
