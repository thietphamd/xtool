<?php
namespace App\FitIn;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class AssetTuVan
{
    protected $api_url;
    
    public function __construct()
    {
        $this->api_url = [
            'projects' => 'api/v1/projects'
        ];
    }

    public function getByType($type, $query = false)
    {
        if(!isset($this->api_url[$type])){
            $string = implode(", ", array_keys($this->api_url));
            return [         
                "code" => -1,
                "message" => "Only support $string",
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "Only support $string",
                        "stack" => []
                    ]
                ]
            
            ];
        }
        $arrResult = self::requestGET($this->api_url[$type], $query);
        
        return $arrResult['data'];    
    }

    public function getByKey($type, $key, $query = false)
    {
        if(!isset($this->api_url[$type])){
            $string = implode(", ", array_keys($this->api_url));
            return [         
                "code" => -1,
                "message" => "Only support $string",
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "Only support $string",
                        "stack" => []
                    ]
                ]
            ];
        }
        $url = str_replace('{key}', $key, $this->api_url[$type]);
        $arrResult = self::requestGET($url, $query);
        
        if(isset($arrResult['code']) && $arrResult['code'] == -1){
            return null;
        }
        return $arrResult['data'];    
    }

   

    public static function requestGET($url, $query = false)
    {
        try {
            $query_params = $query ?  ['query' => $query] : [];
            $url = config('fitin.ecom.tuvan_api_endpoint') . '/' . $url;
            $client = new Client();
            
            $response = $client->request('GET',  $url , $query_params);
            $stream = $response->getBody();
            $contents = $stream->getContents();
            $result = json_decode($contents, 1);
            return $result;
            
        } catch (ClientException $e) {
            return [
                "code" => -1,
                "message" => $e->getMessage(),
                "errors" => [
                    [
                        "error" => 1,
                        "message" => $e->getMessage(),
                        "stack" => []
                    ]
                ]
            ];
        }
    }

    public static function requestPOST($url, $dataPost)
    {
        try {
            $arrData = [
                'headers' => []
            ];
            $arrData['json'] = $dataPost;
            $client = new \GuzzleHttp\Client();
            $url = config('fitin.ecom.tuvan_api_endpoint') . '/' . $url;
            
            $response = $client->request('POST', $url, $arrData);

            if ($response->getStatusCode() == 200) {
                $jsonResult = $response->getBody()->getContents();
                $result = json_decode($jsonResult, 1);

                return $result;
            }
            return false;
        } catch (ClientException $e) {
            return [
                "code" => -1,
                "message" => $e->getMessage(),
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ];
        }
    }

}
