<?php
namespace App\FitIn;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class Consultant
{
    protected $api_url, $token;
    
    public function __construct()
    {
        $this->api_url = [
            #'bundle.show' => 'editor/v2/bundles/{key}', 
            #'bundle.store' => 'editor/v2/bundles', 
            #'room.show' => 'editor/v2/rooms/{key}', 
            #'room.store' => 'editor/v2/rooms', 
            #'layout.show' => 'editor/v2/layouts/{key}', 
            'layout.store' => 'editor/v1/layouts', 
        ];

        $this->token = config('fitin.editor.private_token');
    }

    public function getByType($type, $query = false)
    {
        if(!isset($this->api_url[$type])){
            $string = implode(", ", array_keys($this->api_url));
            return [         
                "code" => -1,
                "message" => "Only support $string",
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "Only support $string",
                        "stack" => []
                    ]
                ]
            
            ];
        }
        $arrResult = $this->requestGET($this->api_url[$type], $query);
        
        return $arrResult['data'];    
    }

    public function getByKey($type, $key, $query = false)
    {
        if(!isset($this->api_url[$type])){
            $string = implode(", ", array_keys($this->api_url));
            return [         
                "code" => -1,
                "message" => "Only support $string",
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "Only support $string",
                        "stack" => []
                    ]
                ]
            ];
        }
        $url = str_replace('{key}', $key, $this->api_url[$type]);
        $arrResult = $this->requestGET($url, $query);
        
        return $arrResult['data'];    
    }

   
    public function createByType($type, $data){
        if(!isset($this->api_url[$type])){
            $string = implode(", ", array_keys($this->api_url));
            return [         
                "code" => -1,
                "message" => "Only support $string",
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "Only support $string",
                        "stack" => []
                    ]
                ]
            
            ];
        }
        $arrResult = $this->requestPOST($this->api_url[$type], $data);
        
        return $arrResult['data'];    
    }

    public function requestGET($url, $query = false)
    {
        try {
            $query_params = $query ? 
            [
                'headers' => [
                    "Access-Key" => $this->token
                ],
                'query' => $query
            ] : 
            [
                'headers' => [
                    "Access-Key" => $this->token
                ]
            ];
            $url = config('fitin.threejs.api_endpoint') . '/' . $url;
            $client = new Client();
            
            $response = $client->request('GET',  $url , $query_params);
            $stream = $response->getBody();
            $contents = $stream->getContents();
            $result = json_decode($contents, 1);
            return $result;
            
        } catch (ClientException $e) {
            return [
                "code" => -1,
                "message" => $e->getMessage(),
                "errors" => [
                    [
                        "error" => 1,
                        "message" => $e->getMessage(),
                        "stack" => []
                    ]
                ]
            ];
        }
    }

    public function requestPOST($url, $dataPost)
    {
        try {
            $arrData = [
                'headers' => [ "Access-Key" => $this->token]
            ];
            $arrData['json'] = $dataPost;
            $client = new \GuzzleHttp\Client();
            $url = config('fitin.threejs.api_endpoint') . '/' . $url;
            
            $response = $client->request('POST', $url, $arrData);
            Log::channel('homestyler-pro')->debug($arrData);
            if ($response->getStatusCode() == 200) {
                $jsonResult = $response->getBody()->getContents();
                $result = json_decode($jsonResult, 1);
                Log::channel('homestyler-pro')->debug($result);
                return $result;
            }
            return false;
        } catch (ClientException $e) {
            
            $log = [
                "code" => -1,
                "message" => $e->getMessage(),
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ];

            Log::channel('homestyler-pro')->debug($log);
        }
    }

}
