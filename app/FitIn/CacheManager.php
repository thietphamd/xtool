<?php

namespace App\FitIn;
use App\Models\V2\Bundle;
use App\Models\V2\Objects;
use App\Models\V2\Sku;
use App\Models\V2\Permission;
use App\Models\V2\Role;
use Illuminate\Support\Facades\Cache;

class CacheManager
{

    public static function clearObjectAndSku()
    {
        Cache::tags(Objects::CACHE_TAG)->flush();
        Cache::tags(Sku::CACHE_TAG)->flush();
    }

    public static function clearBundle()
    {
        Cache::tags(Bundle::CACHE_TAG)->flush();

    }

    //Permission List
    public static function getPermissions($params = []){

        $permissions = Cache::rememberForever(Permission::CACHE_KEY, function () {
            return Permission::all();
        });
        
        foreach ($params as $attr => $value) {
            $permissions = $permissions->where($attr, $value);
        }

        return $permissions;
    }

    //Role List
    public static function getRoles($params = []){
        $roles = Cache::rememberForever(Role::CACHE_KEY, function () {
            return Role::all();
        });

        foreach ($params as $attr => $value) {
            $roles = $roles->where($attr, $value);
        }

        return $roles;
    }

    //Permission of User
    public static function getAllPermissions($user){
        $data = Cache::tags(Permission::CACHE_TAG)->rememberForever($user->id, function() use ($user) {
            $permissions = $user->permissions;
        
            if ($user->roles) {
                $permissions = $permissions->merge($user->getPermissionsViaRoles());
            }
            
            return $permissions->sort()->values();
        });
        return $data;
    }

    public static function forgetCachePermissions(){
        Cache::forget(Permission::CACHE_KEY);
    }

    public static function forgetCacheRoles(){
        Cache::forget(Role::CACHE_KEY);
    }

    public static function forgetCacheUserPermissions(){
        Cache::tags(Permission::CACHE_TAG)->flush();
    }
}
