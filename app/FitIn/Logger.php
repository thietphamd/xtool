<?php

namespace App\FitIn;

use Illuminate\Support\Facades\Log;

class Logger
{

    public static function isDev()
    {
        return env("IS_DEV", false);
    }

    public static function debug($arg)
    {

        if (self::isDev()) {
            Log::channel('slack')->debug($arg);
        }
    }

}
