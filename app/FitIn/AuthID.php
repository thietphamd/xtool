<?php

namespace App\FitIn;

use App\Exceptions\GeneralException;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\Log;

class AuthID
{
    const SESSION_NAME = 'auth_login';

    protected $api_endpoint;
    protected $app_id;
    protected $app_secret;
    private $access_token;

    protected $api_url = [
        'register' => 'api/v1/user/credential/create',
        'login' => 'api/v1/user/credential/auth',
        'forgot_password' => 'api/v1/user/credential/password/forgot',
        'confirmation_resend' => 'api/v1/user/credential/confirmation/resend',
        'unique_email' => 'api/v1/user/credential/email/check',
        'unique_phone' => 'api/v1/user/credential/phone/check',
        'token_verify' => 'api/v1/user/credential/token/verify',
        'user_info' => 'api/v1/user/credential/info',
        'reset_password' => 'api/v1/user/credential/password/reset',
        'google_login' => 'api/v1/user/google/auth',
        'facebook_login' => 'api/v1/user/facebook/auth',
        'update_password' => 'api/v1/user/credential/update/password/svr',
        'verify_account' => 'api/v1/user/credential/confirmation/validate/',
        'upload_avatar' => 'api/v1/user/profile/avatar',
        'get_profile' => 'api/v1/user/profile/get',
        'create_profile' => 'api/v1/user/profile/create',
        'remove_account' => 'api/v1/user/credential/remove',
        'forgot_password_update_password' => 'api/v1/user/credential/password/reset',
        'verify_otp' => 'api/v1/user/credential/opt/checkvalid'
    ];


    static private $_instance = NULL;

    static function getInstance()
    {
        if (self::$_instance == NULL) {
            self::$_instance = new AuthID();
        }
        return self::$_instance;
    }

    public function __construct()
    {
        $this->app_id = config('fitin.auth.app_id');
        $this->app_secret = config('fitin.auth.app_secret');
        $this->api_endpoint = config('fitin.auth.api_endpoint');
    }

    /**
     * @param $token
     */
    public function setToken($token)
    {
        $this->access_token = $token;
    }

    /**
     * @return SessionManager|\Illuminate\Session\Store|mixed
     */
    public function getToken()
    {
        return $this->access_token;
    }

    /**
     * @param $action
     * @return string
     */
    public function getUrl($action)
    {
        return $this->api_endpoint . '/' . $this->api_url[$action];
    }

    /**
     * @param $url
     * @param $dataPost
     * @return bool|mixed
     */
    public function requestPOST($url, $dataPost)
    {
        $arrData = [
            'headers' => [
                'X-App-Id' => $this->app_id,
                'X-App-Secret' => $this->app_secret
            ]
        ];
        $arrData['json'] = $dataPost;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, $arrData);

        if ($response->getStatusCode() == 200) {
            $jsonResult = $response->getBody()->getContents();
            $result = json_decode($jsonResult, 1);

            return $result;
        }
        return false;
    }

    public function requestGET($url, $token)
    {
        $arrData = [
            'headers' => [
                'X-App-Id' => $this->app_id,
                'X-App-Secret' => $this->app_secret,
                'X-Access-Token' => $token
            ]
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, $arrData);


        if ($response->getStatusCode() == 200) {
            $jsonResult = $response->getBody()->getContents();
            $result = json_decode($jsonResult, 1);

            return $result;
        }
        return false;
    }

    public function requestAPI($url, $arrParam)
    {
        $arrData = [
            'headers' => [
                'X-App-Id' => $this->app_id,
                'X-App-Secret' => $this->app_secret,
                'X-Access-Token' => $this->access_token
            ]
        ];

        if(!empty($arrParam['json'])){
            $arrData['json'] = $arrParam['json'];
        }

        if(!empty($arrParam['file'])){
            $file = $arrParam['file'];

            $arrData['multipart'] = [
              [
                  'name' => 'file',
                  'contents' => file_get_contents($file->path()),
                  'filename' => $file->hashName()
              ]
            ];
        }

        //$arrData['debug'] = true;

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, $arrData);


        if ($response->getStatusCode() == 200) {
            $jsonResult = $response->getBody()->getContents();
            $result = json_decode($jsonResult, 1);

            return $result;
        }
        return false;
    }

    /**
     * @param $email
     * @param $password
     * @return bool|mixed
     */
    public function register($email, $password,$phone = null)
    {
        $register_api = $this->getUrl('register');
        $result = $this->requestPOST($register_api, [
            "email" => $email,
            "phone" => $phone,
            "password" => $password
        ]);


        return $result;
    }

    /**
     * @param $email
     * @param $password
     * @return bool
     */
    public function login($email, $password)
    {
        $register_api = $this->getUrl('login');

        $result = $this->requestPOST($register_api, [
            "key" => $email,
            "password" => $password
        ]);

        if ($result && $result['code'] >= 0) {
            return $result['data'];
        }

        return false;
    }

    public function loginByToken( $token)
    {
        $register_api = $this->getUrl('token_verify');

        $result = $this->requestPOST($register_api, [
            "token" => $token
        ]);
           
        if ($result && $result['code'] == 0) {
            return $result['data'];
        }

        return false;
    }


    public function isValidEmail($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     * @param $email
     * @return bool
     */
    public function forgotPassword($email)
    {
        $register_api = $this->getUrl('forgot_password');
        $result = $this->requestPOST($register_api, [
            "key" => $email
        ]);

        return $result && $result['code'] >= 0 ? true : false;
    }

    /**
     * @param $token
     * @param $email
     * @param $new_password
     * @param $retype_new_password
     * @return bool
     */
    public function resetPassword($token, $email, $new_password, $retype_new_password)
    {
        $register_api = $this->getUrl('reset_password');
        $result = $this->requestPOST($register_api, [
            'key' => $email,
            'new_password' => $new_password,
            'retype_new_password' => $retype_new_password,
            'token' => $token
        ]);
        return $result['code'] >= 0 ? true : false;
    }

    /**
     * @param $email
     * @return bool
     */
    public function confirmationResend($email)
    {
        $register_api = $this->getUrl('confirmation_resend');

        $result = $this->requestPOST($register_api, [
            'key' => $email
        ]);

        if ($result && $result['code'] >= 0) {
            return true;
        }

        Log::debug('confirmationResend',$result);

        return false;
    }

    /**
     * @param $email
     * @return bool
     */
    public function hasUniqueEmail($email)
    {
        $register_api = $this->getUrl('unique_email');
        $result = $this->requestPOST($register_api, [
            'email' => $email
        ]);


        if ($result && $result['code'] === -1) {
            return true;
        }

        return false;
    }

    /**
     * @param $email
     * @return bool
     */
    public function hasUniquePhone($phone)
    {
        $register_api = $this->getUrl('unique_phone');

        $result = $this->requestPOST($register_api, [
            'phone' => $phone
        ]);

        if ($result && $result['code'] === -1) {
            return true;
        }
 
        return false;
    }

    public function hasExistEmail($email){
        return $this->hasUniqueEmail($email) ? true : false;
    }

    public function hasExistPhone($phone){
        return $this->hasUniquePhone($phone) ? true : false;
    }

    /**
     * @param $token
     * @return bool
     */
    public function getInfo($token)
    {
        $register_api = $this->getUrl('user_info');
        $result = $this->requestPOST($register_api, [
            'token' => $token
        ]);

        if ($result && $result['code'] >= 0) {
            return $result['data'];
        }
        return false;
    }

    /**
     * @param $token
     * @return bool
     */
    public function hasConfirmation($token)
    {
        $userInfo = $this->getInfo($token);
        return $userInfo && $userInfo['status'] == 1 ? true : false;
    }

    public function socialLogin($social, $access_token)
    {
        $register_api = $this->getUrl($social . '_login');
        $result = $this->requestPOST($register_api, [
            'access_token' => $access_token
        ]);

        if ($result && !empty($result['token'])) {
            return $result;
        }

        return false;
    }

    /**
     * @param $email
     * @param $new_password
     * @param $retype_new_password
     * @return bool
     * @throws GeneralException
     */
    public function changePassword($email, $new_password, $retype_new_password)
    {
        $update_password = $this->getUrl('update_password');
        $result = $this->requestPOST($update_password, [
            'email' => $email,
            'new_password' => $new_password,
            'retype_new_password' => $retype_new_password
        ]);
        if ($result['code'] >= 0) {
            return true;
        } else {
            throw new GeneralException($result['message']);
        }
    }


    public function removeAccount($email)
    {
        $remove_account = $this->getUrl('remove_account');

        $result = $this->requestPOST($remove_account, [
            'email' => $email
        ]);


        if ($result && $result['code'] >= 0) {
            return true;
        } else {
            throw new GeneralException($result['message']);
        }
    }

    /**
     * @param $email
     * @param $token
     * @return bool
     * @throws GeneralException
     */
    public function verifyAccount($email, $code)
    {
        $verify_account = $this->getUrl('verify_account');

        $result = $this->requestPOST($verify_account, [
            'key' => $email,
            'token' => $code
        ]);


        if ($result['code'] >= 0) {
            return true;
        } else {
           return false;
        }
    }

    public function checkOtpValid($email,$otp){
        $verify_account = $this->getUrl('verify_otp');

        $result = $this->requestPOST($verify_account, [
            'key' => $email,
            'otp' => $otp
        ]);


        if ((bool)$result['data']) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $file
     * @return bool
     * @throws GeneralException
     */
    public function uploadAvatar($file)
    {
        $upload_avatar = $this->getUrl('upload_avatar');

        $result = $this->requestAPI($upload_avatar, [
            'file' => $file
        ]);
        //dd($result);
        if ($result['code'] >= 0) {
            return true;
        } else {
            throw new GeneralException($result['message']);
        }
    }

    public function refreshToken($old_token)
    {

    }

    public function getProfile($access_token)
    {

        $update_password = $this->getUrl('get_profile');
        $result = $this->requestGET($update_password, $access_token);
        return $result;
    }

    public function tokenVerify($access_token)
    {
        $api = $this->getUrl('token_verify');

        $result = $this->requestPOST($api, [
            'token' => $access_token
        ]);


        if ($result['code'] >= 0) {
            return $result['data'];
        } else {
            return false;
        }
    }
}
