<?php

namespace App\Providers;

use App\Events\CreateTenantUser;
use App\Listeners\CreateTenantUserListener;

use App\Events\UploadEvent;
use App\Listeners\UploadEventListener;

use App\Events\RequestEvent;
use App\Listeners\RequestEventListener;


use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        CreateTenantUser::class => [
            CreateTenantUserListener::class
        ],
        UploadEvent::class => [
            UploadEventListener::class
        ],
        RequestEvent::class => [
            RequestEventListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
