<?php

namespace App\Providers;

use App\Models\Ver2\User;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Services\Auth\AuthService;
use App\Services\Auth\AccessTokenGuard;
use App\Services\Auth\EditorAuthService;
use App\Services\Auth\EditorAccessTokenGuard;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [    
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::extend('access_token', function ($app, $name, array $config) {
            $userProvider = app(AuthService::class);
            $request = app('request');
            return new AccessTokenGuard($userProvider, $request, $config);
        });

        Auth::extend('editor_access_token', function ($app, $name, array $config) {
            $editorProvider = app(EditorAuthService::class);
            $request = app('request');
            return new EditorAccessTokenGuard($editorProvider, $request, $config);
        });
    }
}
