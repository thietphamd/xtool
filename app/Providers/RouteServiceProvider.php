<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace_app = 'App\Http\Controller\App';
    protected $namespace_app_master = 'App\Http\Controllers\AppMaster';
    protected $namespace_web = 'App\Http\Controllers\Web';
    protected $namespace_cms_tenant = 'App\Http\Controllers\Cms';
    protected $namespace_cms_master = 'App\Http\Controllers\CmsMaster';
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {

        //Use for Rebuild version
        
        $this->mapAppRoutes();
        $this->mapAppMasterRoutes();
        $this->mapCmsMasterRoutes();
        $this->mapCmsTenantRoutes();
        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace_web)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */


    protected function mapAppRoutes()
    {
        Route::namespace($this->namespace_app)
             ->group(base_path('routes/app.php'));
    }

    protected function mapAppMasterRoutes()
    {
        Route::namespace($this->namespace_app_master)
             ->group(base_path('routes/app_master.php'));
    }

    protected function mapCmsTenantRoutes()
    {
        Route::namespace($this->namespace_cms_tenant)
             ->group(base_path('routes/cms_tenant.php'));
    }

    protected function mapCmsMasterRoutes()
    {
        Route::namespace($this->namespace_cms_master)
             ->group(base_path('routes/cms_master.php'));
    }

}
