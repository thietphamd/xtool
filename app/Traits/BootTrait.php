<?php

namespace App\Traits;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Models\Ver2\Bundle;

trait BootTrait
{
    use CacheTrait;

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            
        });

        self::created(function ($model) {
            self::process($model, "create");
        });

        self::updating(function ($model) {

        });

        self::updated(function ($model) {
            self::process($model, "update");
        });

        self::saving(function ($model) {

        });

        self::saved(function ($model) {
            self::process($model, "update");
        });


        self::deleting(function ($model) {

        });

        self::deleted(function ($model) {
            self::process($model, "delete");
        });
    }

    public static function process($model, $action)
    {
        self::flushCache();
        if(config('fitin.enabled_sync_to_external')){
            self::postWebhook($model, $action);
        }
        
    }

    public static function postWebhook($model, $action)
    {
        $class = get_class($model);
        $model_action = \App\Models\Activity\ModelAction::where('model', $class)->where('action', 'like', '%.'. $action)->first();
        if(!$model_action){
            return false;
        }
        
        if(!$model_action->param){
            $resource = \App\Webhook\Models\UserWebhookResource::with('user')->where('model', $class)->where('action', $model_action->action)->first();  
            if($resource){
                $user = $resource->user;
                if($user && $user->active && $user->webhook){
                    //$data = $model->toArray();
                    $data = self::transformData($class, $model);
                    $webhook = new \App\Webhook\Services\Webhook($user->webhook, $resource->type, $resource->action, $data);
                    $webhook->post();

                }   
            }
               
        }else{
            $resources = \App\Webhook\Models\UserWebhookResource::with('user')->where('model', $class)->where('key', $model->getKey())->where('action', $model_action->action)->get();
            foreach($resources as $resource){
                $user = $resource->user;
                if($user && $user->active && $user->webhook){
                    //$data = $model->toArray();
                    $data = self::transformData($class, $model);
                    
                    $webhook = new \App\Webhook\Services\Webhook($user->webhook, $resource->type, $resource->action, $data);
                    $webhook->post();
    
                }
            }
        
        }
        
        

    }

    public static function transformData($class, $model){
        if($class == Bundle::class){
            $data = [
                '_id' => $model->_id,
                'name' => $model->name,
                'category' => $model->category,
                'vendor' => $model->vendor,
                'room' => $model->room,
                'brand' => $model->brand,
                'key_name' => $model->key_name,
                'status' => $model->status,
                
                'item_list' => $model->item_list,
                'imagePath' => $model->imagePath,
                'image' => $model->image,
                
                'settings' => $model->settings,
                'attrs' => $model->attrs
            ];

            return $data;

        }else{
            return $model->toArray();
        }

    }
}
