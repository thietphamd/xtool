<?php 

namespace App\Traits;

trait AttributeSchemaTrait
{
    public function setAttribute($key, $value)
    {
        if (!empty($this->attributes_schema)){
            $schema_key_array = array_keys($this->attributes_schema);
            
            if(in_array($key, $schema_key_array)){
                $type = $this->attributes_schema[$key];
                if($value && gettype($value) !== $type){
                    
                    throw new \Exception("Invalid type $key. Must be $type");
                }
            }
        }

        if (!empty($this->strtolower)){
            $strtolower_array = $this->strtolower;
            
            if(in_array($key, $strtolower_array)){             
                $value = strtolower($value);
            }
        }
        parent::setAttribute($key, $value);
        
    }
}