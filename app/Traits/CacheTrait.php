<?php

namespace App\Traits;

use Illuminate\Support\Facades\Cache;

trait CacheTrait
{
    public static function bootCacheTrait()
    {
       // parent::boot();

        self::creating(function ($model) {
            
        });

        self::created(function ($model) {
            self::flushCache();
        });

        self::updating(function ($model) {

        });

        self::updated(function ($model) {
            self::flushCache();
        });

        self::deleting(function ($model) {

        });

        self::deleted(function ($model) {
            self::flushCache();
        });
    }

    public static function flushCache()
    {   
        $cacheModel = [
            \App\Models\Library::class,
            \App\Models\Object3d::class,
            \App\Models\Bundle::class,
            \App\Models\Room::class,
            \App\Models\Project::class,
            \App\Models\Style::class,
            \App\Models\Layout::class,
            \App\Models\Material::class,
            \App\Models\User::class
        ];

         
        if(in_array(self::class, $cacheModel)){
            foreach($cacheModel as $model){           
                if(defined("$model::CACHE_TAG")){   
                    Cache::tags($model::CACHE_TAG)->flush();
                }   
            } 
        }else{
            if(defined("self::CACHE_TAG")){   
                Cache::tags(self::CACHE_TAG)->flush();
            }  

            if(defined("self::ADDITIONAL_CACHE_TAG")){   
                Cache::tags(self::ADDITIONAL_CACHE_TAG)->flush();
            }  
        }
        
    }
}
