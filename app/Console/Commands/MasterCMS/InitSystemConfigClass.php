<?php

namespace App\Console\Commands\MasterCMS;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\SystemConfig;
use Illuminate\Support\Facades\Hash;

class InitSystemConfigClass extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:master:system:config:class';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $configLists = config('general.config');
        foreach ($configLists as $type=>$configs){
            foreach ($configs as $config){
                $class = [
                    'name' => $config['name'],
                    'code' => $config['key'],
                    'type' => $type,
                    'status' => SystemConfig::ACTIVE
                ];
                SystemConfig::updateOrCreate(['code' => $class['code']], $class);
            }
           
        }

    }
}
