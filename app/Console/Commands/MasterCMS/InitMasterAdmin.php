<?php

namespace App\Console\Commands\MasterCMS;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\CMSMaster;
use Illuminate\Support\Facades\Hash;

class InitMasterAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:master:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $user = [
            'name' => 'RA Master',
            'email' => 'admin@rollingant.com',
            'password' => '123456',
          
            'active' => true
        ];
        CMSMaster::updateOrCreate(['email' => $user['email']], $user);

        $user = [
            'name' => 'Fitin Master',
            'email' => 'admin@fitin.vn',
            'password' => '123456',
         
            'active' => true
        ];
        CMSMaster::updateOrCreate(['email' => $user['email']], $user);
    }
}
