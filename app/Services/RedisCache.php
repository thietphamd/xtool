<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Carbon;

class RedisCache
{
   

    public function __construct()
    {
       
    }

    public static function buildKey($class, $key){
        $tag = $class::CACHE_TAG;
        $user = Auth::user();
        $prefix = $user ? $user->tenant_id : 'raau';
        return "$prefix:$tag:$key";
    }

    public static function buildHash($class){
        $tag = $class::CACHE_TAG;
        $user = Auth::user();
        $prefix = $user ? $user->tenant_id : 'raau';
        return "$prefix:$tag";
    }

    public static function hget($hash, $key){
        $data = \Redis::hGet($hash, $key);
        return unserialize($data);
    }

    public static function hset($hash, $key, $value){
        $value = serialize($value);
        return \Redis::hSet($hash, $key, $value);
    }

    public static function hdel($hash, $key){
        return \Redis::hDel($hash, $key);
    }

    public static function get($key){
        $data = \Redis::get($key);
        return unserialize($data);
    }

    public static function set($key, $value, $type = null, $expire = null){
        $value = serialize($value);
        if($type && $expire){
            return \Redis::set($key, $value, $type, $expire);
        }
        return \Redis::set($key, $value);
    }
}
