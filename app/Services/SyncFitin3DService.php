<?php

namespace App\Services;

use App\Models\CMSMaster;

use Illuminate\Support\Facades\Cache;

class SyncFitin3DService
{
    protected $fitin3d_endpoint;

    protected $api = [
        'model3d' => 'v1/model3d',
        'category' => 'v1/category'
    ];

    public function __construct()
    {
        $this->fitin3d_endpoint = config('general');
    }

    public function get($params = []){

       
    }

    /**
    * @param $url
    * @param $dataPost
    * @return bool|mixed
    */
    public function requestPOST($url, $dataPost)
    {
        try {
            $arrData = [
                'headers' => [
                    'access-key' => $this->access_token
                ]
            ];
            $arrData['json'] = $dataPost;
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', $url, $arrData);
            
            if ($response->getStatusCode() == 200) {
                $jsonResult = $response->getBody()->getContents();
                $result = json_decode($jsonResult, 1);
                    
                return $result;
            }
            return false;
        } catch (\Exception $e) {
            $arrError = [
                'api' => config('app.url'),
                'error' => class_basename($e) . ' | ' .$e->getMessage(),
                'stack' => mb_substr($e->getTraceAsString(), 0, 150)  
            ];
            $channel = env('LOG_CHANNEL', 'stack');
            Log::channel($channel)->error($arrError);
            return false;
        }
    }


    public function requestGET($url, $query = false)
    {
        try {
            $query_params = $query ?  ['query' => $query] : [];
            $url = $this->fitin3d_endpoint . '/' . $url;
            $client = new Client();
            
            $response = $client->request('GET',  $url , $query_params);
            $stream = $response->getBody();
            $contents = $stream->getContents();
            $result = json_decode($contents, 1);
            return $result;
            
        } catch (\Exception $e) {
            $arrError = [
                'api' => config('app.url'),
                'error' => class_basename($e) . ' | ' .$e->getMessage(),
                'stack' => mb_substr($e->getTraceAsString(), 0, 150)  
            ];
            $channel = env('LOG_CHANNEL', 'stack');
            Log::channel($channel)->error($arrError);
            return false;
        }
    }
}
