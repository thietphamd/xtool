<?php

namespace App\Http\Middleware;

use Closure;

class HandleStatus
{
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->setStatusCode(200);
    }
}
