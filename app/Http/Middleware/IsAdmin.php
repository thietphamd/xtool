<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if(!$user->hasRole('admin')){
            return response()->json([
                'code' => -401,
                'message' => 'Your account has no access to this site!'
            ]);
        }
        return $next($request);
    }
}
