<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Ver2\User;

class PrivateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(request()->header('fitin-token') &&  in_array(request()->header('fitin-token'), config('external-token') )){         
            $email = array_search (request()->header('fitin-token'), config('external-token'));
            $user = User::where('email', $email)->first();
            if(!$user){
                return response()->json([
                    'code' => -1,
                    'message' => 'User not exists!',
                    "errors" => [
                        [
                            "error" => 1,
                            "message" => "",
                            "stack" => []
                        ]
                    ]
                ]);
            }
            $request->merge(['userId' => $user->auth_id]);
            return $next($request);
        }else{
            return response()->json([
                'code' => -1,
                'message' => 'Unauthorized!',
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }
        
    }
}
