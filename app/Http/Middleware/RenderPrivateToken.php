<?php

namespace App\Http\Middleware;

use Closure;


class RenderPrivateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(request()->header('render-token') && request()->header('render-token') == config()->get('fitin.render_token')){         
            
            return $next($request);
        }else{
            return response()->json([
                'code' => -1,
                'message' => 'Unauthorized!',
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }
        
    }
}
