<?php

namespace App\Http\Requests\AR;

use App\Rules\EmailExist;
use App\Rules\PhoneExist;
use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|min:6|same:password',
            'email' => ['required', 'email', new EmailExist],
            'phone' => ['required','phone:AUTO', new PhoneExist]
        ];
    }
}
