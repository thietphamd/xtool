<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MaterialBrandResourceCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\MaterialBrandResource';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
