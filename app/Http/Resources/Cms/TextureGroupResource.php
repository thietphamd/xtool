<?php

namespace App\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class TextureGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            'tenant_id' => $this->tenant_id,
            'description' => $this->description,
            'parent_id' => $this->parent_id,
            'children' => new SubTextureGroupResourceCollection($this->sub_groups),
            'imageUrl' => $this->imageUrl
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
