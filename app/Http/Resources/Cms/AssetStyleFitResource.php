<?php

namespace App\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\AssetStyleFit;

class AssetStyleFitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $default_attributes = AssetStyleFit::DEFAULT_ASSET_ATTRIBUTES;
        $assetStyleFit_attributes = $this->getOriginal('attributes') ?? [];
        $final_attributes = array_merge($default_attributes, $assetStyleFit_attributes);
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            'imageUrl' => $this->imageUrl,
            'category_id' => $this->category_id,
            'brand_id' => $this->brand_id,
            'category' => $this->category,
            'brand' => $this->brand,
            'description' => $this->description,
            'status' => $this->status,
            'sku' => $this->sku,
            'attributes' => $final_attributes,
            'extensions' => $this->extensions
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
