<?php

namespace App\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            'status' => $this->status,
            'tenant_id' => $this->tenant_id,
            'builder_id' => $this->builder_id,
            'builder' => $this->builder,
            'plan_id' => $this->plan_id,
            'plan' => $this->plan,
            //'tenant' => $this->tenant,
            'imageUrl' => $this->imageUrl
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
