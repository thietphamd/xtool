<?php

namespace App\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class MaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            '_id' => $this->_id,
            'name' => $this->name,
            'imageUrl' => $this->imageUrl,
            'category_id' => $this->category_id,
            'brand_id' => $this->brand_id,
            'category' => $this->category,
            'brand' => $this->brand,
            'description' => $this->description,
            'status' => $this->status,
            'tags' => $this->tags ?? [],
            'extensions' => $this->extensions
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
