<?php

namespace App\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class PlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            'builder_id' => $this->builder_id,
            'builder' => $this->builder,
            'status' => $this->status,
            'tenant_id' => $this->tenant_id,
            //'tenant' => $this->tenant,
            'imageUrl' => $this->imageUrl,
            'metadata' => $this->metadata
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
