<?php

namespace App\Http\Resources\CmsMaster;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tenant = $this->tenant ?? null;
        
        return [
            '_id' => $this->_id,
            'tenant' => $tenant ? [
                'name' => $tenant->name,
                'email' => $tenant->email
            ] : null,
            'tenant_id' => $this->tenant_id,
            "subscription_plan" => $this->subscription_plan,
            "subscription_plan_id" => $this->subscription_plan_id,
            "price" => $this->price,
            "recurring_month" => $this->recurring_month,
            "status" => $this->status
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
