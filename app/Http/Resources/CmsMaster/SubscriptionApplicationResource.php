<?php

namespace App\Http\Resources\CmsMaster;

use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            "description" => $this->description,
            'features' => $this->features ?? []
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
