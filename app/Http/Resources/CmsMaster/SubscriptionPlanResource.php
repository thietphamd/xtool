<?php

namespace App\Http\Resources\CmsMaster;

use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            "description" => $this->description,
            "applications" => $this->applications,
            "options" => $this->options
            //'recurring_month' => $this->recurring_month,
            //'price' => $this->price
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
