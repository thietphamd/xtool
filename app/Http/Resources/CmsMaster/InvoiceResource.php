<?php

namespace App\Http\Resources\CmsMaster;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tenant = $this->tenant ?? null;
        $payment = $this->payment ?? null;
        return [
            '_id' => $this->_id,
            'tenant' => $tenant ? [
                'name' => $tenant->name,
                'email' => $tenant->email
            ] : null,
            "payment" => $payment ? [
                'price' => $payment->price
            ] : ['price' => 0],
            "tenant_id" => $this->tenant_id,
            "payment_id" => $this->payment_id,
            "created_at" => $this->created_at
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
