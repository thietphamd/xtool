<?php

namespace App\Http\Resources\CmsMaster;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TenantResourceCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\CmsMaster\TenantResource';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
