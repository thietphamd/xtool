<?php

namespace App\Http\Resources\AppMaster;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\TenantUser;

class AssetRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $model = $this->model;
        switch ($model){
            case 'App\\Models\\Material' :
                $model = 'material';
                break;
            case 'App\\Models\\Asset' :
                $model = 'asset';
                break;
            case 'App\\Models\\AssetStyleFit' :
                $model = 'stylefit';
                break;
            default;
                break;
        };
        
        
        return [
            "_id" => $this->_id,
            "tenant_id" => $this->tenant_id,
            "model" => $model,
            "model_id" => $this->key,
            "requested_by" => ($this->requester) ? $this->requester->email : '',
            "finished_by" => ($this->finisher) ? $this->finisher->email : '',
            "status" => $this->status,
            'tenant' => $this->tenant,
            "created_at" =>$this->created_at,
            "requestable" => $this->requestable
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
