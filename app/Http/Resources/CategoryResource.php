<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Object3d;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $object_attributes = ($this->object_attributes && is_array($this->object_attributes) && !empty($this->object_attributes)) ? array_merge( Object3d::DEFAULT_OBJECT_ATTRIBUTES, $this->object_attributes) : Object3d::DEFAULT_OBJECT_ATTRIBUTES;
        foreach($object_attributes as $key=>$att){
            if($att === false){
                $object_attributes[$key] = 0;
            }
            if($att === true){
                $object_attributes[$key] = 1;
            }
        }
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            "code" => $this->code,
            'status' => $this->status,
            'image' => $this->imageUrl,
            'priority' => $this->priority,
            'sub_categories' => new SubCategoryResourceCollection($this->sub_categories),
            'attributes' => $object_attributes
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
