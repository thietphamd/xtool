<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Cms\BuilderResourceCollection;
use App\Http\Resources\Cms\BuilderResource;

use App\Models\Builder;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\BuilderRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class BuilderController extends ApiController
{
    protected $builderRepository;

    public function __construct(BuilderRepository $builderRepository){
        $this->builderRepository = $builderRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return BuilderCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $builders = $this->builderRepository->getAll($params);
        return new BuilderResourceCollection($builders);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }

        $builder = $this->builderRepository->create($input);
           
        return new BuilderResource($builder);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return BuilderResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $builder = $this->builderRepository->find($id);
        if(!$builder){
            return $this->responseError('Builder not found' );
        }
        return new BuilderResource($builder);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return BuilderResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['tenant_id']);
        $builder = $this->builderRepository->update($key, $input);
        if(!$builder){
            return $this->responseError('Builder not found' );
        }
        
        return new BuilderResource($builder);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $builder = $this->builderRepository->delete($key);
        if(!$builder){
            return $this->responseError('Builder not found' );
        }
        return $this->respondSuccess();
    }


    /** *****************************************************************************
     * Upload Builder Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadThumb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Builder::DIR_NAME;
        $thumb_dir = Builder::THUMB_DIR;
        $file = $request->file('image');
      
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $builder = $this->builderRepository->find($id);
        if(!$builder){
            return $this->responseError('Builder not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$tenant_id/$type/$thumb_dir/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $builder->image = "$path/$file_base_name";
            $builder->save();
            $builder->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'model' => get_class($builder),
                'key' => $builder->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $builder);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
    
}
