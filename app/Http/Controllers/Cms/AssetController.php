<?php

namespace App\Http\Controllers\Cms;

use App\Models\Asset;
use App\Models\Brand;
use App\Models\Category;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Repositories\AssetRepository;
use App\Repositories\BrandRepository;
use App\Repositories\CategoryRepository;

use App\Http\Resources\Cms\AssetResource;
use App\Http\Resources\Cms\AssetResourceCollection;
use App\Http\Resources\Cms\BrandResourceCollection;
use App\Http\Resources\Cms\CategoryResourceCollection;

use App\Http\Controllers\ApiController;

class AssetController extends ApiController
{
    protected $assetRepository, $brandRepository, $categoryRepository;

    public function __construct(
        AssetRepository $assetRepository,
        BrandRepository $brandRepository,
        CategoryRepository $categoryRepository
        ){
        $this->assetRepository = $assetRepository;
        $this->brandRepository = $brandRepository;
        $this->categoryRepository = $categoryRepository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
    
        $assets = $this->assetRepository->getAll($params);
        return new AssetResourceCollection($assets);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return AssetResource
     */
    public function show(Request $request, $key)
    {
        $asset = $this->assetRepository->find($key);
        if(!$asset){
            return $this->responseError('Asset not found' );
        }
        return new AssetResource($asset);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        if (preg_match('/[\'^£$%&*}{@#~?><>,|=+¬]/', $input['name']))
        {
            return $this->responseError("name contains invalid character", "INVALID"); 
        }
        
        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }

        $asset = $this->assetRepository->create($input);

        return new AssetResource($asset);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return AssetResource|JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input = $request->all();
        $asset = $this->assetRepository->update($key, $input);
        if(!$asset){
            return $this->responseError('Asset not found' );
        }
        
        return new AssetResource($asset);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     */
    public function destroy(Request $request,$key)
    {
        $asset = $this->assetRepository->delete($key);
        if(!$asset){
            return $this->responseError('Asset not found' );
        }
        return $this->respondSuccess();
    }

    /**
     * Get List Category Of Asset
     */
    public function getCategories(Request $request)
    {
        $params = $request->all();
        $params['class'] = Asset::CONFIG_TYPE;
        $categories = $this->categoryRepository->getAll($params);
        return new CategoryResourceCollection($categories);
    }

    /**
     * Get List Brand Of Asset
     */
    public function getBrands(Request $request)
    {
        $params = $request->all();
        $params['class'] = Asset::CONFIG_TYPE;
        $brands = $this->brandRepository->getAll($params);
        return new BrandResourceCollection($brands);
    }
    
    /**
     * Get List Asset Attributes
     */
    public function getAttributes(Request $request)
    {
        return $this->responseObjectData(Asset::buildListAttributes());
    }


    /** *****************************************************************************
     * Upload Asset Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadThumb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Asset::DIR_NAME;
        $dir = Asset::THUMB_DIR;
        $file = $request->file('image');
      
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $asset = $this->assetRepository->find($id);
        if(!$asset){
            return $this->responseError('Asset not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $asset->image = "$path/$file_base_name";
            $asset->save();
            $asset->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'model' => get_class($asset),
                'key' => $asset->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $asset);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Asset Asset Bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadAssetBundle(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Asset::DIR_NAME;
        $dir = Asset::AB_DIR;
        $file = $request->file('file');
      
        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $asset = $this->assetRepository->find($id);
        if(!$asset){
            return $this->responseError('Asset not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            $time = \Carbon\Carbon::now()->timestamp;
            $extensions = $asset->getOriginal('extensions') ?? [];
            $extensions['ab'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $asset->extensions = $extensions;
            $asset->save();
            $asset->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'model' => get_class($asset),
                'key' => $asset->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $asset);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Asset Glb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadGlb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Asset::DIR_NAME;
        $dir = Asset::GLB_DIR;
        $file = $request->file('file');
      
        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $asset = $this->assetRepository->find($id);
        if(!$asset){
            return $this->responseError('Asset not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            $time = \Carbon\Carbon::now()->timestamp;
            $extensions = $asset->getOriginal('extensions') ?? [];
            $extensions['glb'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $asset->extensions = $extensions;
            $asset->save();
            $asset->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'model' => get_class($asset),
                'key' => $asset->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $asset);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Asset Zip.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadZip(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Asset::DIR_NAME;
        $dir = Asset::ZIP_DIR;
        $file = $request->file('file');
      
        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $asset = $this->assetRepository->find($id);
        if(!$asset){
            return $this->responseError('Asset not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            $time = \Carbon\Carbon::now()->timestamp;
            $extensions = $asset->getOriginal('extensions') ?? [];
            $extensions['zip'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $asset->extensions = $extensions;
            $asset->save();
            $asset->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'model' => get_class($asset),
                'key' => $asset->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $asset);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /**
     * Create request a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function storeRequest(Request $request)
    {
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'id'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $id = $input['id'];

        $asset = $this->assetRepository->find($id);
        if(!$asset){
            return $this->responseError('NOT FOUND', "INVALID");  
        }
        $event = event(new \App\Events\RequestEvent($asset, Auth::user()));

        return $this->responseSuccess(['id' => $id]);
    }
}
