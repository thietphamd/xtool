<?php

namespace App\Http\Controllers\Cms;


use App\Http\Resources\Cms\TenantUserResourceCollection;
use App\Http\Resources\Cms\TenantUserResource;

use App\Models\TenantUser;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\TenantUserRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;


class TenantUserController extends ApiController
{
    protected $tenantUserRepository;

    public function __construct(TenantUserRepository $tenantUserRepository){
        $this->tenantUserRepository = $tenantUserRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return TenantCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $tenantUsers = $this->tenantUserRepository->getAll($params);
        return new TenantUserResourceCollection($tenantUsers);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email|unique:TenantUsers,email',
            'password'   => 'required|string|min:6|max:14'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

            
        $input['email'] = strtolower($input['email']);
        $tenantUsers = $this->tenantUserRepository->find($input['email']);
        if($tenantUsers) {
            return $this->responseError('Tenant Already Existed', 'DATA_EXISTS');
        }
        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }
        $tenantUsers = $this->tenantUserRepository->create($input);          

        return new TenantUserResource($tenantUsers);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return TenantResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $tenantUsers = $this->tenantUserRepository->find($id);
        if(!$tenantUsers){
            return $this->responseError('Tenant not found' );
        }
        return new TenantUserResource($tenantUsers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return TenantResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['email', 'password']);
        $tenantUsers = $this->tenantUserRepository->update($key, $input);
        if(!$tenantUsers){
            return $this->responseError('Tenant not found' );
        }
        
        return new TenantUserResource($tenantUsers);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $tenantUsers = $this->tenantUserRepository->delete($key);
        if(!$tenantUsers){
            return $this->responseError('Tenant not found' );
        }
        return $this->respondSuccess();
    }

    public function changePassword(Request $request, $id)
    {
        $input = $request->only(['password', 'password_confirmation']);
        $validator = Validator::make($input, [         
            'password'   => 'required|string|min:6|max:14|confirmed'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $params = ['password' => (string) $input['password']];
            
        $tenantUsers = $this->tenantUserRepository->update($id, $params);
        if(!$tenantUsers){
            return $this->responseError('Account not found' );
        }
            
        return new TenantUserResource($tenantUsers);
        
    }
}
