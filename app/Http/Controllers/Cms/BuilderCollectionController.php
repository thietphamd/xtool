<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Cms\BuilderCollectionResourceCollection;
use App\Http\Resources\Cms\BuilderCollectionResource;

use App\Models\BuilderCollection;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\BuilderCollectionRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class BuilderCollectionController extends ApiController
{
    protected $builderCollectionRepository;

    public function __construct(BuilderCollectionRepository $builderCollectionRepository){
        $this->builderCollectionRepository = $builderCollectionRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return BuilderCollectionCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $builderCollections = $this->builderCollectionRepository->getAll($params);
        return new BuilderCollectionResourceCollection($builderCollections);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }

        $builderCollection = $this->builderCollectionRepository->create($input);
           
        return new BuilderCollectionResource($builderCollection);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return BuilderCollectionResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $builderCollection = $this->builderCollectionRepository->find($id);
        if(!$builderCollection){
            return $this->responseError('BuilderCollection not found' );
        }
        return new BuilderCollectionResource($builderCollection);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return BuilderCollectionResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['tenant_id']);
        $builderCollection = $this->builderCollectionRepository->update($key, $input);
        if(!$builderCollection){
            return $this->responseError('BuilderCollection not found' );
        }
        
        return new BuilderCollectionResource($builderCollection);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $builderCollection = $this->builderCollectionRepository->delete($key);
        if(!$builderCollection){
            return $this->responseError('BuilderCollection not found' );
        }
        return $this->respondSuccess();
    }


    /** *****************************************************************************
     * Upload BuilderCollection Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadThumb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = BuilderCollection::DIR_NAME;
        $thumb_dir = BuilderCollection::THUMB_DIR;
        $file = $request->file('image');
      
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $builderCollection = $this->builderCollectionRepository->find($id);
        if(!$builderCollection){
            return $this->responseError('BuilderCollection not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$tenant_id/$type/$thumb_dir/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $builderCollection->image = "$path/$file_base_name";
            $builderCollection->save();
            $builderCollection->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'model' => get_class($builderCollection),
                'key' => $builderCollection->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $builderCollection);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
    
}
