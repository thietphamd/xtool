<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Cms\TextureResourceCollection;
use App\Http\Resources\Cms\TextureResource;

use App\Models\Texture;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\TextureRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class TextureController extends ApiController
{
    protected $textureRepository;

    public function __construct(TextureRepository $textureRepository){
        $this->textureRepository = $textureRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return TextureCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $textures = $this->textureRepository->getAll($params);
        return new TextureResourceCollection($textures);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'image'    => 'required|mimes:jpg,png,jpeg'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $file = $request->file('image');
        if (!$file || !$file->isValid()){
            return $this->responseError('Upload failed', "INVALID");
        }

        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }

        $texture = $this->textureRepository->create($input);
        $request->request->add(['id' => $texture->_id]);
        return $this->uploadThumb($request);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return TextureResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $texture = $this->textureRepository->find($id);
        if(!$texture){
            return $this->responseError('Texture not found' );
        }
        return new TextureResource($texture);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return TextureResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['tenant_id']);
        $texture = $this->textureRepository->update($key, $input);
        if(!$texture){
            return $this->responseError('Texture not found' );
        }
        
        return new TextureResource($texture);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $texture = $this->textureRepository->delete($key);
        if(!$texture){
            return $this->responseError('Texture not found' );
        }
        return $this->respondSuccess();
    }


    /** *****************************************************************************
     * Upload Texture Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadThumb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Texture::DIR_NAME;
        $thumb_dir = Texture::THUMB_DIR;
        $file = $request->file('image');
      
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $texture = $this->textureRepository->find($id);
        if(!$texture){
            return $this->responseError('Texture not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            
            $path = "$tenant_id/$type/$thumb_dir";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $texture->path = "$path/$file_base_name";
            $texture->save();
            $texture->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'model' => get_class($texture),
                'key' => $texture->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $texture);

            return new TextureResource($texture);;
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
    
}
