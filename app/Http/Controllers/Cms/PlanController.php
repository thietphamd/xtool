<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Cms\PlanResourceCollection;
use App\Http\Resources\Cms\PlanResource;

use App\Models\Plan;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\PlanRepository;
use App\Repositories\BuilderRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class PlanController extends ApiController
{
    protected $planRepository, $builderRepository;

    public function __construct(PlanRepository $planRepository, BuilderRepository $builderRepository){
        $this->planRepository = $planRepository;
        $this->builderRepository = $builderRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return PlanCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $plans = $this->planRepository->getAll($params);
        return new PlanResourceCollection($plans);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        if (!empty($input['builder_id'])){
            $builder = $this->builderRepository->find($input['builder_id']);
            if(!$builder){
                return $this->responseError('Builder not found' );  
            }
        }
        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }

        $plan = $this->planRepository->create($input);
           
        return new PlanResource($plan);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return PlanResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $plan = $this->planRepository->find($id);
        if(!$plan){
            return $this->responseError('Plan not found' );
        }
        return new PlanResource($plan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return PlanResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['tenant_id']);
        $plan = $this->planRepository->update($key, $input);
        if(!$plan){
            return $this->responseError('Plan not found' );
        }
        
        return new PlanResource($plan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $plan = $this->planRepository->delete($key);
        if(!$plan){
            return $this->responseError('Plan not found' );
        }
        return $this->respondSuccess();
    }


    /** *****************************************************************************
     * Upload Plan Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadThumb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Plan::DIR_NAME;
        $thumb_dir = Plan::THUMB_DIR;
        $file = $request->file('image');
      
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $plan = $this->planRepository->find($id);
        if(!$plan){
            return $this->responseError('Plan not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$tenant_id/$type/$thumb_dir/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $plan->image = "$path/$file_base_name";
            $plan->save();
            $plan->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'model' => get_class($plan),
                'key' => $plan->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $plan);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
    
}
