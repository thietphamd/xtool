<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Cms\TextureGroupResourceCollection;
use App\Http\Resources\Cms\TextureGroupResource;

use App\Models\TextureGroup;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\TextureGroupRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class TextureGroupController extends ApiController
{
    protected $textureGroupRepository;

    public function __construct(TextureGroupRepository $textureGroupRepository){
        $this->textureGroupRepository = $textureGroupRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return TextureGroupCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $textureGroups = $this->textureGroupRepository->getAll($params);
        return new TextureGroupResourceCollection($textureGroups);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }

        $textureGroup = $this->textureGroupRepository->create($input);
           
        return new TextureGroupResource($textureGroup);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return TextureGroupResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $textureGroup = $this->textureGroupRepository->find($id);
        if(!$textureGroup){
            return $this->responseError('TextureGroup not found' );
        }
        return new TextureGroupResource($textureGroup);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return TextureGroupResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['tenant_id']);
        $textureGroup = $this->textureGroupRepository->update($key, $input);
        if(!$textureGroup){
            return $this->responseError('TextureGroup not found' );
        }
        
        return new TextureGroupResource($textureGroup);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $textureGroup = $this->textureGroupRepository->delete($key);
        if(!$textureGroup){
            return $this->responseError('TextureGroup not found' );
        }
        return $this->respondSuccess();
    }


    /** *****************************************************************************
     * Upload TextureGroup Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadThumb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = TextureGroup::DIR_NAME;
        $thumb_dir = TextureGroup::THUMB_DIR;
        $file = $request->file('image');
      
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $textureGroup = $this->textureGroupRepository->find($id);
        if(!$textureGroup){
            return $this->responseError('TextureGroup not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            
            $path = "$tenant_id/$type/$thumb_dir";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $textureGroup->image = "$path/$file_base_name";
            $textureGroup->save();
            $textureGroup->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'model' => get_class($textureGroup),
                'key' => $textureGroup->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $textureGroup);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
    
}
