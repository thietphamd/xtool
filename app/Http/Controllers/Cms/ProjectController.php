<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Cms\ProjectResourceCollection;
use App\Http\Resources\Cms\ProjectResource;

use App\Models\Project;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\ProjectRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class ProjectController extends ApiController
{
    protected $projectRepository;

    public function __construct(ProjectRepository $projectRepository){
        $this->projectRepository = $projectRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return ProjectCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $projects = $this->projectRepository->getAll($params);
        return new ProjectResourceCollection($projects);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }

        $project = $this->projectRepository->create($input);
           
        return new ProjectResource($project);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return ProjectResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $project = $this->projectRepository->find($id);
        if(!$project){
            return $this->responseError('Project not found' );
        }
        return new ProjectResource($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return ProjectResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['tenant_id']);
        $project = $this->projectRepository->update($key, $input);
        if(!$project){
            return $this->responseError('Project not found' );
        }
        
        return new ProjectResource($project);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $project = $this->projectRepository->delete($key);
        if(!$project){
            return $this->responseError('Project not found' );
        }
        return $this->respondSuccess();
    }


    /** *****************************************************************************
     * Upload Project Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadThumb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Project::DIR_NAME;
        $thumb_dir = Project::THUMB_DIR;
        $file = $request->file('image');
      
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $project = $this->projectRepository->find($id);
        if(!$project){
            return $this->responseError('Project not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$tenant_id/$type/$thumb_dir/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $project->image = "$path/$file_base_name";
            $project->save();
            $project->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'model' => get_class($project),
                'key' => $project->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $project);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
    
}
