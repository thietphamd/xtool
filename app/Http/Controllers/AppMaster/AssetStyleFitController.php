<?php

namespace App\Http\Controllers\AppMaster;

use App\Models\AssetStyleFit;
use App\Models\Brand;
use App\Models\Category;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Repositories\AssetStyleFitRepository;
use App\Repositories\BrandRepository;
use App\Repositories\CategoryRepository;

use App\Http\Resources\Cms\AssetStyleFitResource;
use App\Http\Resources\Cms\AssetStyleFitResourceCollection;
use App\Http\Resources\Cms\BrandResourceCollection;
use App\Http\Resources\Cms\CategoryResourceCollection;

use App\Http\Controllers\ApiController;

class AssetStyleFitController extends ApiController
{
    protected $assetStyleFitRepository, $brandRepository, $categoryRepository;

    public function __construct(
        AssetStyleFitRepository $assetStyleFitRepository,
        BrandRepository $brandRepository,
        CategoryRepository $categoryRepository
        ){
        $this->assetStyleFitRepository = $assetStyleFitRepository;
        $this->brandRepository = $brandRepository;
        $this->categoryRepository = $categoryRepository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
    
        $assetStyleFits = $this->assetStyleFitRepository->getAll($params);
        return new AssetStyleFitResourceCollection($assetStyleFits);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return AssetStyleFitResource
     */
    public function show(Request $request, $key)
    {
        $assetStyleFit = $this->assetStyleFitRepository->find($key);
        if(!$assetStyleFit){
            return $this->responseError('AssetStyleFit not found' );
        }
        return new AssetStyleFitResource($assetStyleFit);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        if (preg_match('/[\'^£$%&*}{@#~?><>,|=+¬]/', $input['name']))
        {
            return $this->responseError("name contains invalid character", "INVALID"); 
        }
        
        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }

        $assetStyleFit = $this->assetStyleFitRepository->create($input);

        return new AssetStyleFitResource($assetStyleFit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return AssetStyleFitResource|JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input = $request->all();
        $assetStyleFit = $this->assetStyleFitRepository->update($key, $input);
        if(!$assetStyleFit){
            return $this->responseError('AssetStyleFit not found' );
        }
        
        return new AssetStyleFitResource($assetStyleFit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     */
    public function destroy(Request $request,$key)
    {
        $assetStyleFit = $this->assetStyleFitRepository->delete($key);
        if(!$assetStyleFit){
            return $this->responseError('AssetStyleFit not found' );
        }
        return $this->respondSuccess();
    }

    /**
     * Get List Category Of AssetStyleFit
     */
    public function getCategories(Request $request)
    {
        $params = $request->all();
        $params['class'] = AssetStyleFit::CONFIG_TYPE;
        $categories = $this->categoryRepository->getAll($params);
        return new CategoryResourceCollection($categories);
    }

    /**
     * Get List Brand Of AssetStyleFit
     */
    public function getBrands(Request $request)
    {
        $params = $request->all();
        $params['class'] = AssetStyleFit::CONFIG_TYPE;
        $brands = $this->brandRepository->getAll($params);
        return new BrandResourceCollection($brands);
    }
    
    /**
     * Get List AssetStyleFit Attributes
     */
    public function getAttributes(Request $request)
    {
        return $this->responseObjectData(AssetStyleFit::buildListAttributes());
    }


    /** *****************************************************************************
     * Upload AssetStyleFit Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadThumb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = AssetStyleFit::DIR_NAME;
        $dir = AssetStyleFit::THUMB_DIR;
        $file = $request->file('image');
      
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $assetStyleFit = $this->assetStyleFitRepository->find($id);
        if(!$assetStyleFit){
            return $this->responseError('AssetStyleFit not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $assetStyleFit->image = "$path/$file_base_name";
            $assetStyleFit->save();
            $assetStyleFit->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'model' => get_class($assetStyleFit),
                'key' => $assetStyleFit->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $assetStyleFit);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload AssetStyleFit AssetStyleFit Bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadAssetStyleFitBundle(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = AssetStyleFit::DIR_NAME;
        $dir = AssetStyleFit::AB_DIR;
        $file = $request->file('file');
      
        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $assetStyleFit = $this->assetStyleFitRepository->find($id);
        if(!$assetStyleFit){
            return $this->responseError('AssetStyleFit not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            $time = \Carbon\Carbon::now()->timestamp;
            $extensions = $assetStyleFit->getOriginal('extensions') ?? [];
            $extensions['ab'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $assetStyleFit->extensions = $extensions;
            $assetStyleFit->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'model' => get_class($assetStyleFit),
                'key' => $assetStyleFit->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $assetStyleFit);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload AssetStyleFit Glb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadGlb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = AssetStyleFit::DIR_NAME;
        $dir = AssetStyleFit::GLB_DIR;
        $file = $request->file('file');
      
        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $assetStyleFit = $this->assetStyleFitRepository->find($id);
        if(!$assetStyleFit){
            return $this->responseError('AssetStyleFit not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            $time = \Carbon\Carbon::now()->timestamp;
            $extensions = $assetStyleFit->getOriginal('extensions') ?? [];
            $extensions['glb'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $assetStyleFit->extensions = $extensions;
            $assetStyleFit->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'model' => get_class($assetStyleFit),
                'key' => $assetStyleFit->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $assetStyleFit);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload AssetStyleFit Zip.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadZip(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = AssetStyleFit::DIR_NAME;
        $dir = AssetStyleFit::ZIP_DIR;
        $file = $request->file('file');
      
        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $assetStyleFit = $this->assetStyleFitRepository->find($id);
        if(!$assetStyleFit){
            return $this->responseError('AssetStyleFit not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            $time = \Carbon\Carbon::now()->timestamp;
            $extensions = $assetStyleFit->getOriginal('extensions') ?? [];
            $extensions['zip'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $assetStyleFit->extensions = $extensions;
            $assetStyleFit->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'model' => get_class($assetStyleFit),
                'key' => $assetStyleFit->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $assetStyleFit);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    
}
