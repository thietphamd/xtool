<?php

namespace App\Http\Controllers\AppMaster;

use App\Models\Material;
use App\Models\Brand;
use App\Models\Category;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Repositories\MaterialRepository;
use App\Repositories\BrandRepository;
use App\Repositories\CategoryRepository;

use App\Http\Resources\Cms\MaterialResource;
use App\Http\Resources\Cms\MaterialResourceCollection;
use App\Http\Resources\Cms\BrandResourceCollection;
use App\Http\Resources\Cms\CategoryResourceCollection;

use App\Http\Controllers\ApiController;

class MaterialController extends ApiController
{
    protected $materialRepository, $brandRepository, $categoryRepository;

    public function __construct(
        MaterialRepository $materialRepository,
        BrandRepository $brandRepository,
        CategoryRepository $categoryRepository
        ){
        $this->materialRepository = $materialRepository;
        $this->brandRepository = $brandRepository;
        $this->categoryRepository = $categoryRepository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
    
        $materials = $this->materialRepository->getAll($params);
        return new MaterialResourceCollection($materials);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return MaterialResource
     */
    public function show(Request $request, $key)
    {
        $material = $this->materialRepository->find($key);
        if(!$material){
            return $this->responseError('Material not found' );
        }
        return new MaterialResource($material);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        if (preg_match('/[\'^£$%&*}{@#~?><>,|=+¬]/', $input['name']))
        {
            return $this->responseError("name contains invalid character", "INVALID"); 
        }
        
        $user = Auth::user();
        if($user->tenant_id){
            $input['tenant_id'] = $user->tenant_id;
        }

        $material = $this->materialRepository->create($input);

        return new MaterialResource($material);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return MaterialResource|JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input = $request->all();
        $material = $this->materialRepository->update($key, $input);
        if(!$material){
            return $this->responseError('Material not found' );
        }
        
        return new MaterialResource($material);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     */
    public function destroy(Request $request,$key)
    {
        $material = $this->materialRepository->delete($key);
        if(!$material){
            return $this->responseError('Material not found' );
        }
        return $this->respondSuccess();
    }

    /**
     * Get List Category Of Material
     */
    public function getCategories(Request $request)
    {
        $params = $request->all();
        $params['class'] = Material::CONFIG_TYPE;
        $categories = $this->categoryRepository->getAll($params);
        return new CategoryResourceCollection($categories);
    }

    /**
     * Get List Brand Of Material
     */
    public function getBrands(Request $request)
    {
        $params = $request->all();
        $params['class'] = Material::CONFIG_TYPE;
        $brands = $this->brandRepository->getAll($params);
        return new BrandResourceCollection($brands);
    }
    

    /** *****************************************************************************
     * Upload Material Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadThumb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Material::DIR_NAME;
        $dir = Material::THUMB_DIR;
        $file = $request->file('image');
      
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $material = $this->materialRepository->find($id);
        if(!$material){
            return $this->responseError('Material not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $material->image = "$path/$file_base_name";
            $material->save();
            $material->updateCache();
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'model' => get_class($material),
                'key' => $material->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $material);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Material Asset Bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadAssetBundle(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Material::DIR_NAME;
        $dir = Material::AB_DIR;
        $file = $request->file('file');
      
        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $material = $this->materialRepository->find($id);
        if(!$material){
            return $this->responseError('Material not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            $time = \Carbon\Carbon::now()->timestamp;
            $extensions = $material->getOriginal('extensions') ?? [];
            $extensions['ab'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $material->extensions = $extensions;
            $material->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'model' => get_class($material),
                'key' => $material->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $material);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Material Glb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadGlb(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Material::DIR_NAME;
        $dir = Material::GLB_DIR;
        $file = $request->file('file');
      
        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $material = $this->materialRepository->find($id);
        if(!$material){
            return $this->responseError('Material not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            $time = \Carbon\Carbon::now()->timestamp;
            $extensions = $material->getOriginal('extensions') ?? [];
            $extensions['glb'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $material->extensions = $extensions;
            $material->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'model' => get_class($material),
                'key' => $material->_id,
                'tenant_id' => $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $material);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Material Zip.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadZip(Request $request){
        $driver = config('general.master.filesystem_driver');
        $user = Auth::user();
        $tenant_id = $user->tenant_id;
       
        $type = Material::DIR_NAME;
        $dir = Material::ZIP_DIR;
        $file = $request->file('file');
      
        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            'id'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $id = $request->get('id');
        $material = $this->materialRepository->find($id);
        if(!$material){
            return $this->responseError('Material not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$tenant_id/$type/$dir/$id";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            $time = \Carbon\Carbon::now()->timestamp;
            $extensions = $material->getOriginal('extensions') ?? [];
            $extensions['zip'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $material->extensions = $extensions;
            $material->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'model' => get_class($material),
                'key' => $material->_id,
                'tenant_id' => $material->tenant_id ?? $tenant_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $material);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /**
     * Create request a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function storeRequest(Request $request)
    {
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'id'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $id = $input['id'];

        $material = $this->materialRepository->find($id);
        if(!$material){
            return $this->responseError('NOT FOUND', "INVALID");  
        }
        $event = event(new \App\Events\RequestEvent($material, Auth::user()));

        return $this->responseSuccess(['id' => $id]);
    }
}
