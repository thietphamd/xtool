<?php

namespace App\Http\Controllers\App;

use App\FitIn\EditorAuthID as AuthID;

use App\Models\Project;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ProjectRepository;

use App\Http\Resources\ProjectResource;
use App\Http\Resources\ProjectResourceCollection;

class ProjectController extends ApiController
{
    protected $repository;

    public function __construct(ProjectRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $projects = $this->repository->getAll($params);
        return new ProjectResourceCollection($projects);
    }

}
