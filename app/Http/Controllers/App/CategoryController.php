<?php

namespace App\Http\Controllers\App;

use App\FitIn\EditorAuthID as AuthID;

use App\Models\Category;
use App\Models\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Repositories\CategoryRepository;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\CategoryResourceCollection;

class CategoryController extends ApiController
{
    protected $repository;

    public function __construct(CategoryRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $categories = $this->repository->getAll($params);
        return new CategoryResourceCollection($categories);
    }

}
