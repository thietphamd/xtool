<?php

namespace App\Http\Controllers\App;


use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return response()->json([
            'message' => 'Welcome to Raau CMS Api',
            'version' => '1.0.0',
            'date' => '2020-10-19'
        ]);
    }

}
