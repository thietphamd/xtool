<?php

namespace App\Http\Controllers\CmsMaster;


use App\Http\Resources\CmsMaster\InvoiceResourceCollection;
use App\Http\Resources\CmsMaster\InvoiceResource;

use App\Models\Invoice;
use App\Models\Payment;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\InvoiceRepository;
use App\Repositories\PaymentRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

use PDF;

class InvoiceController extends ApiController
{
    protected $invoiceRepository, $paymentRepository;

    public function __construct(InvoiceRepository $invoiceRepository, PaymentRepository $paymentRepository){
        $this->invoiceRepository = $invoiceRepository;
        $this->paymentRepository = $paymentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return InvoiceCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $invoices = $this->invoiceRepository->getAll($params);
        return new InvoiceResourceCollection($invoices);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'tenant_id' => 'required',
            'payment_id' => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $payment_id = $input['payment_id'];
        $payment = $this->paymentRepository->find($payment_id);
        if(!$payment){
            return $this->responseError('Payment not found' );
        }
        $invoice = $this->invoiceRepository->create($input);
        $this->paymentRepository->update($payment_id, ['status' => Payment::SUCCESS]);
           
        return new InvoiceResource($invoice);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return InvoiceResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $invoice = $this->invoiceRepository->find($id);
        if(!$invoice){
            return $this->responseError('Invoice not found' );
        }
        return new InvoiceResource($invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return InvoiceResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->only(['status']);
        $invoice = $this->invoiceRepository->update($key, $input);
        if(!$invoice){
            return $this->responseError('Invoice not found' );
        }
        
        return new InvoiceResource($invoice);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $invoice = $this->invoiceRepository->delete($key);
        if(!$invoice){
            return $this->responseError('Invoice not found' );
        }
        return $this->respondSuccess();
    }

    /**
     * Download PDF.
     *
     * @param  string $id
     * @return Stream|Response
     */
    public function downloadPDF(Request $request, $id)
    {
        $invoice = $this->invoiceRepository->find($id);
        if(!$invoice){
            return $this->responseError('Invoice not found' );
        }
        $invoice->load(['payment', 'tenant', 'payment.subscription_plan']);
       
        view()->share('invoice', $invoice);
        $pdf = PDF::loadView('templates.pdf.invoice', $invoice);

        return $pdf->download('pdf_file.pdf');
    }
}
