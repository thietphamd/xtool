<?php

namespace App\Http\Controllers\CmsMaster;


use App\Http\Resources\AppMaster\AssetRequestResourceCollection;
use App\Http\Resources\AppMaster\AssetRequestResource;

use App\Models\AssetRequest;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\AssetRequestRepository;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class AssetRequestController extends ApiController
{
    protected $assetRequestRepository;

    public function __construct(AssetRequestRepository $assetRequestRepository){
        $this->assetRequestRepository = $assetRequestRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return AssetRequestResourceCollection
     */
    public function index(Request $request)
    {   
        $params = $request->all();
        $reqs = $this->assetRequestRepository->getAll($params);
        return new AssetRequestResourceCollection($reqs);
    }
   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return AssetResource
     */
    public function show(Request $request, $id)
    {
        $req = $this->assetRequestRepository->find($key);
        if(!$req){
            return $this->responseError('Request not found' );
        }
        return new AssetRequestResourceCollection($req);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return AssetRequestResource|JsonResponse
     * @throws AuthorizationException
     */
    public function updateRejected(Request $request, $id)
    {
        $input = [
            'status' => AssetRequest::REJECTED
        ];

        $req = $this->assetRequestRepository->update($id, $input);
        if(!$req){
            return $this->responseError('Request not found' );
        }
        
        return new AssetRequestResource($req);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return AssetRequestResource|JsonResponse
     * @throws AuthorizationException
     */
    public function updateFinished(Request $request, $id)
    {
        $input = [
            'status' => AssetRequest::FINISHED
        ];

        $req = $this->assetRequestRepository->update($id, $input);
        if(!$req){
            return $this->responseError('Request not found' );
        }
        
        return new AssetRequestResource($req);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$id)
    {
        $req = $this->assetRequestRepository->delete($id);
        if(!$req){
            return $this->responseError('Request not found' );
        }
        return $this->responseSuccess(['id' => $id]);
    }

    
}
