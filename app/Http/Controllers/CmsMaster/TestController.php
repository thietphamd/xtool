<?php

namespace App\Http\Controllers\CmsMaster;

use App\Models\Invoice;
use App\Models\Payment;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\ApiController;
use App\Models\Material;
use Redis;
use RedisCache;

class TestController extends ApiController
{
    
    public function __construct(){
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return 
     */
    public function index(Request $request)
    {
        $redis = new Redis();
        $data = Material::first();
        Redis::hSet("material", "abc", serialize($data));
        Redis::hSet("material", "abd", serialize($data));
        Redis::hSet("material", "abe", serialize($data));

        
        dd(hexdec($data->_id));
        dd(Redis::zRangeByScore('materiales', 0, 3));

        Redis::zAdd("materiales",  1, serialize($data));

        Redis::sAdd('materialsorts', 1);
        dd(Redis::sort('materialsorts'));
        // Redis::zAdd("materials",  1, $data);
        // Redis::zAdd("materials",  2, $data);
        // Redis::zAdd("materials",  3, $data);
        
        dd(Redis::zRangeByScore('materiales', 0, 3));
    }

    
    
}
