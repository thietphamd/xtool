<?php

namespace App\Http\Controllers\CmsMaster;


use App\Http\Resources\CmsMaster\SubscriptionPlanResourceCollection;
use App\Http\Resources\CmsMaster\SubscriptionPlanResource;

use App\Models\SubscriptionPlan;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\SubscriptionPlanRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class SubscriptionPlanController extends ApiController
{
    protected $subscriptionPlanRepository;

    public function __construct(SubscriptionPlanRepository $subscriptionPlanRepository){
        $this->subscriptionPlanRepository = $subscriptionPlanRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return SubscriptionPlanCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $subscriptionPlans = $this->subscriptionPlanRepository->getAll($params);
        
        return new SubscriptionPlanResourceCollection($subscriptionPlans);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|string',
            'description' => 'required|string'
            //'price' => 'required',
            //'recurring_month' => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        

        $subscriptionPlan = $this->subscriptionPlanRepository->create($input);

        if(!empty($input['applications'])){
            $subscriptionPlan->applications()->sync($input['applications']);
            $subscriptionPlan->touch();
        }
           
        if(!empty($input['options']) && is_array($input['options'])){
            $subscriptionPlan = $this->storeOptions($input['options'], $subscriptionPlan->_id);
        }
        return new SubscriptionPlanResource($subscriptionPlan);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return SubscriptionPlanResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $subscriptionPlan = $this->subscriptionPlanRepository->find($id);
        if(!$subscriptionPlan){
            return $this->responseError('SubscriptionPlan not found' );
        }
        return new SubscriptionPlanResource($subscriptionPlan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return SubscriptionPlanResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['options']);
        $subscriptionPlan = $this->subscriptionPlanRepository->update($key, $input);
        if(!$subscriptionPlan){
            return $this->responseError('SubscriptionPlan not found' );
        }
        if(!empty($input['applications'])){
            $subscriptionPlan->applications()->sync($input['applications']);
            $subscriptionPlan->touch();
        }
        return new SubscriptionPlanResource($subscriptionPlan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $subscriptionPlan = $this->subscriptionPlanRepository->delete($key);
        if(!$subscriptionPlan){
            return $this->responseError('SubscriptionPlan not found' );
        }
        return $this->respondSuccess();
    }

    /**
     * Store a embedsMany  resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    protected function storeOptions($options, $id)
    {
       
        foreach ($options as $option){
            $plan = $this->subscriptionPlanRepository->addOption($id, $option);
        }
        
        return $plan;
    }

    /**
     * Store a embedsMany  resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function storeOption(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'recurring_month' => 'required',
            'price' => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }


        $plan = $this->subscriptionPlanRepository->addOption($id, $input);
        if(!$plan){
            return $this->responseError('SubscriptionPlan not found' );
        }
        return new SubscriptionPlanResource($plan);
    }

    /**
     * update a embedsMany  resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function updateOption(Request $request, $id, $option_id)
    {
        $input = $request->all();
        

        $plan = $this->subscriptionPlanRepository->updateOption($id, $option_id, $input);
        if(!$plan){
            return $this->responseError('SubscriptionPlan not found' );
        }
        return new SubscriptionPlanResource($plan);
    }

    /**
     * delete a embedsMany  resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function destroyOption(Request $request, $id, $option_id)
    {

        $plan = $this->subscriptionPlanRepository->removeOption($id, $option_id);
        if(!$plan){
            return $this->responseError('SubscriptionPlan not found' );
        }
        return new SubscriptionPlanResource($plan);
    }
}
