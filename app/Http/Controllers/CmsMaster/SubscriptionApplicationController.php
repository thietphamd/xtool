<?php

namespace App\Http\Controllers\CmsMaster;


use App\Http\Resources\CmsMaster\SubscriptionApplicationResourceCollection;
use App\Http\Resources\CmsMaster\SubscriptionApplicationResource;

use App\Models\SubscriptionApplication;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\SubscriptionApplicationRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class SubscriptionApplicationController extends ApiController
{
    protected $subscriptionApplicationRepository;

    public function __construct(SubscriptionApplicationRepository $subscriptionApplicationRepository){
        $this->subscriptionApplicationRepository = $subscriptionApplicationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return SubscriptionApplicationCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $subscriptionApplications = $this->subscriptionApplicationRepository->getAll($params);
        return new SubscriptionApplicationResourceCollection($subscriptionApplications);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|string',
            'description' => 'required|string'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        

        $subscriptionApplication = $this->subscriptionApplicationRepository->create($input);
           
        return new SubscriptionApplicationResource($subscriptionApplication);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return SubscriptionApplicationResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $subscriptionApplication = $this->subscriptionApplicationRepository->find($id);
        if(!$subscriptionApplication){
            return $this->responseError('SubscriptionApplication not found' );
        }
        return new SubscriptionApplicationResource($subscriptionApplication);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return SubscriptionApplicationResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->all();
        $subscriptionApplication = $this->subscriptionApplicationRepository->update($key, $input);
        if(!$subscriptionApplication){
            return $this->responseError('SubscriptionApplication not found' );
        }
        
        return new SubscriptionApplicationResource($subscriptionApplication);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $subscriptionApplication = $this->subscriptionApplicationRepository->delete($key);
        if(!$subscriptionApplication){
            return $this->responseError('SubscriptionApplication not found' );
        }
        return $this->respondSuccess();
    }

    
}
