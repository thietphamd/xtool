<?php

namespace App\Http\Controllers\CmsMaster;


use App\Http\Resources\CmsMaster\TenantResourceCollection;
use App\Http\Resources\CmsMaster\TenantResource;

use App\Models\Tenant;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\TenantRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

use App\Events\CreateTenantUser;


class TenantController extends ApiController
{
    protected $tenantRepository;

    public function __construct(TenantRepository $tenantRepository){
        $this->tenantRepository = $tenantRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return TenantCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $tenants = $this->tenantRepository->getAll($params);
        return new TenantResourceCollection($tenants);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email|unique:Tenants,email',
            'password'   => 'required|string|min:6|max:14'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

            
        $input['email'] = strtolower($input['email']);
        $tenant = $this->tenantRepository->find($input['email']);
        if($tenant) {
            return $this->responseError('Tenant Already Existed', 'DATA_EXISTS');
        }

        $tenant = $this->tenantRepository->create($input);
           
        $tenant->password = $input['password'];
        $event = event(new CreateTenantUser($tenant));

        return new TenantResource($tenant);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return TenantResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $tenant = $this->tenantRepository->find($id);
        if(!$tenant){
            return $this->responseError('Tenant not found' );
        }
        return new TenantResource($tenant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return TenantResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['email']);
        $tenant = $this->tenantRepository->update($key, $input);
        if(!$tenant){
            return $this->responseError('Tenant not found' );
        }
        
        return new TenantResource($tenant);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $tenant = $this->tenantRepository->delete($key);
        if(!$tenant){
            return $this->responseError('Tenant not found' );
        }
        return $this->respondSuccess();
    }

    
}
