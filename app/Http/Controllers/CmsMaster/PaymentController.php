<?php

namespace App\Http\Controllers\CmsMaster;


use App\Http\Resources\CmsMaster\PaymentResourceCollection;
use App\Http\Resources\CmsMaster\PaymentResource;

use App\Models\Payment;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\PaymentRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class PaymentController extends ApiController
{
    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository){
        $this->paymentRepository = $paymentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return PaymentCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $payments = $this->paymentRepository->getAll($params);
        return new PaymentResourceCollection($payments);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'tenant_id' => 'required',
            'subscription_plan_id' => 'required',
            'price' => 'required',
            'recurring_month' => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        
        $payment = $this->paymentRepository->create($input);
           
        return new PaymentResource($payment);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return PaymentResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $payment = $this->paymentRepository->find($id);
        if(!$payment){
            return $this->responseError('Payment not found' );
        }
        return new PaymentResource($payment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return PaymentResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->only(['status']);
        $payment = $this->paymentRepository->update($key, $input);
        if(!$payment){
            return $this->responseError('Payment not found' );
        }
        
        return new PaymentResource($payment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $payment = $this->paymentRepository->delete($key);
        if(!$payment){
            return $this->responseError('Payment not found' );
        }
        return $this->respondSuccess();
    }

    
}
