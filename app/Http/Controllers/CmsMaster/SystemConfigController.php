<?php

namespace App\Http\Controllers\CmsMaster;


use App\Http\Resources\CmsMaster\SystemConfigResourceCollection;
use App\Http\Resources\CmsMaster\SystemConfigResource;

use App\Models\SystemConfig;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Repositories\SystemConfigRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;

class SystemConfigController extends ApiController
{
    protected $systemConfigRepository;

    public function __construct(SystemConfigRepository $systemConfigRepository){
        $this->systemConfigRepository = $systemConfigRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return SystemConfigCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $systemConfigs = $this->systemConfigRepository->getAll($params);
        return new SystemConfigResourceCollection($systemConfigs);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'code' => 'required|unique:SystemConfigs,code',
            'type' => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        
        $systemConfig = $this->systemConfigRepository->create($input);
           
        return new SystemConfigResource($systemConfig);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return SystemConfigResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $systemConfig = $this->systemConfigRepository->find($id);
        if(!$systemConfig){
            return $this->responseError('SystemConfig not found' );
        }
        return new SystemConfigResource($systemConfig);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return SystemConfigResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->only(['status', 'name']);
        $systemConfig = $this->systemConfigRepository->update($key, $input);
        if(!$systemConfig){
            return $this->responseError('SystemConfig not found' );
        }
        
        return new SystemConfigResource($systemConfig);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $systemConfig = $this->systemConfigRepository->delete($key);
        if(!$systemConfig){
            return $this->responseError('SystemConfig not found' );
        }
        return $this->respondSuccess();
    }

    public function getListTypes(){
        $list = SystemConfig::LIST_TYPE;
        return $this->responseData($list);
    }

    public function getListClasses(){
        $params = [
            'type' => SystemConfig::TYPE_RESOURCE_CLASS
        ];

        $systemConfigs = $this->systemConfigRepository->getAll($params);
        return new SystemConfigResourceCollection($systemConfigs);
    }
    
}
