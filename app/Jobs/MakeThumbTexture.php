<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use \App\Helpers\Utils;
use App\Models\Ver2\Texture;
use App\Models\Ver2\Object3d;

class MakeThumbTexture implements ShouldQueue
{
   use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

   /**
    * Create a new job instance.
    *
    * @return void
    */
   public $tries = 5;

   protected $texture;

   public function __construct($texture)
   {
      //
      $this->texture = $texture;
     
   }

   /**
    * Execute the job.
    *
    * @return void
    */
   public function handle()
   {
      $texture = $this->texture;
      
      $sizes = Texture::THUMB_SIZE;
        $driver = config('fitin.asset.filesystem_driver');
        foreach ($sizes as $size => $arrAttr) {
            $image_path = Storage::disk($driver)->path(Utils::locationPath(Object3d::DIR_NAME, $texture->brand, $texture->group, Object3d::TEXTURE_DIR, $texture->file));
            
            if(!file_exists($image_path)){
                continue;
            }
            $oImageManager = Image::make($image_path);
            $originWidth = $oImageManager->width();
            if($arrAttr['width'] >= $originWidth && $arrAttr['width'] > Texture::THUMB_DEFAULT_SIZE){
                continue;
            }
            $higher = ($arrAttr['width'] >= $arrAttr['height']) ? $arrAttr['width'] : $arrAttr['height'];
            $oImageManager->fit($higher, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
      
            $oImageManager->stream();
            
            $arrFileName = pathinfo($texture->file);
            $folderThumb = $arrAttr['width'];

            $newFileName = $arrFileName['filename'] . "." . Texture::THUMB_DEFAULT_MIME;
            $pathUpload = Utils::locationPath(Object3d::DIR_NAME, $texture->brand, $texture->group, Object3d::TEXTURE_DIR, $folderThumb,  $newFileName);
            
            Storage::disk($driver)->put($pathUpload, $oImageManager);
            
            uploadDriver($newFileName, $driver, $pathUpload , Storage::disk($driver)->url($pathUpload), $texture);

        }
      
   }
}
