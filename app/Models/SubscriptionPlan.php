<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class SubscriptionPlan extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'SubscriptionPlans';
    protected $fillable = [
        "name" ,
        "description",
        "status",
        // "price",
        // "recurring_month"
    ];
    const INDEXES = ['name'];
    public const PENDING = "pending";
    public const ACTIVE = "active";
    protected $hidden = ['application_ids'];

    public const CACHE_TAG = 'subs_plan';

    
    public function applications() {
        return $this->belongsToMany( SubscriptionApplication::class, 'null' ,'plan_ids',
        'application_ids' );
    }

    public function options() {
        return $this->embedsMany( SubscriptionPlanOption::class);
    }
}