<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Traits\CacheTrait;

class AssetRequest extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'AssetRequests';
    protected $fillable = [
        "tenant_id",
        "model" ,
        "key",
        "requested_by",
        "finished_by",
        "status"
    ];
    const INDEXES = ['tenant_id', 'key', 'created_at'];
    public const PENDING = "pending";
    public const REJECTED = "rejected";
    public const FINISHED = "finished";

    const DIR_NAME = "asset-requests";
    public const CACHE_TAG = 'asset-requests';
   
    const LIST_STATUS = [
        self::PENDING,
        self::REJECTED,
        self::FINISHED
    ];

    public static function boot()
    {
        parent::boot();

        // static::addGlobalScope('withTenant', function ($builder) {
        //     $user = Auth::user();
        //     if($user){
        //         $builder->where('tenant_id', $user->tenant_id);
        //     }
        // });

    }

    public function requester(){
        return $this->belongsTo(TenantUser::class, 'requested_by', 'auth_id')->withoutGlobalScopes();
    }

    public function finisher(){
        return $this->belongsTo(CMSMaster::class, 'finished_by', 'auth_id')->withoutGlobalScopes();
    }

    public function tenant(){
        return $this->belongsTo(Tenant::class, 'tenant_id', '_id');
    }

    public function requestable()
    {
        return $this->morphTo(null, 'model', 'key')->withoutGlobalScopes();
    }
}