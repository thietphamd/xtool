<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;
use Illuminate\Support\Facades\Cache;

class SystemConfig extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'SystemConfigs';
    protected $fillable = [
        "name" ,
        "code",
        "type", 
        "description",
        "status"
    ];
    const INDEXES = ['name'];
    const TYPE_RESOURCE_CLASS = "resource_class";

    const LIST_TYPE = [
        self::TYPE_RESOURCE_CLASS
    ];
    public const PENDING = "pending";
    public const ACTIVE = "active";
    public const CACHE_TAG = 'system_config';

    public static function getResourceClasses(){
        $key = "index_system_classes";
        return Cache::tags(self::CACHE_TAG)->rememberForever($key, function(){
            $list = SystemConfig::where('type', self::TYPE_RESOURCE_CLASS)->where('status', self::ACTIVE)->get();
            $data = [];
            foreach ($list as $class){
                $data[] = [
                    'name' => $class->name,
                    'code' => $class->code,
                    'value' => 0
                ];
            }
            return $data;
        });
    }
}