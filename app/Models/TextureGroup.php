<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Traits\CacheTrait;

class TextureGroup extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'TextureGroups';
    protected $fillable = [
        "tenant_id",
        "parent_id",
        "name" ,
        "description",
        "image"
    ];
    const INDEXES = ['name', 'created_at'];

    const DIR_NAME = "texture-groups";
    public const CACHE_TAG = 'texture-group';
    const THUMB_DIR = "thumb";
   
    
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('withTenant', function ($builder) {
            $user = Auth::user();
            if($user){
                $builder->where('tenant_id', $user->tenant_id);
            }
        });

    }

    public function getImageUrlAttribute()
    {
        $driver = config('general.master.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('general.default_asset_image');
    }

    public function tenant(){
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }

    public function sub_groups(){
        return $this->hasMany(TextureGroup::class, 'parent_id', '_id');
    }

    public function parent(){
        return $this->belongsTo(TextureGroup::class, 'parent_id', '_id');
    }
}