<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FileDriver extends Eloquent {

    protected $collection = 'FileDrivers';

    protected $fillable = [
        'size',
        'type',
        'origin_name',
        'revision',
        "file" ,
        "driver",
        "path",
        "url" ,
        "key",
        "model",
        'tenant_id'
    ];
    
}