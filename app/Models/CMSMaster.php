<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\Traits\HasRoles;
use App\Traits\CacheTrait;
use Illuminate\Support\Facades\Storage;

use App\Models\AutoIncrement;

class CMSMaster extends BaseModel implements JWTSubject, AuthenticatableContract, AuthorizableContract {

    use HasRoles, Authenticatable, Authorizable, CacheTrait;

    const CACHE_TAG = "cms-masters";
    const WORKSPACE_DIR = "workspace/master";
    const THUMB_DIR = "thumb";
    

    protected $collection = 'CMSMasters';

    protected $guarded = ['_id'];

    protected $fillable = [
        'name',
        'email',
        'password',
        'auth_id',
        'active',
        'avatar'
    ];
    

    protected $hidden = [
        'password',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->attributes['auth_id'] = AutoIncrement::getIncrementId('CMSMasters');
        });
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'auth_token' => $this->getAuthToken()
        ];
    }

    public function setAuthToken($token){
        $this->auth_token = $token;
    }

    public function getAuthToken(){
        return $this->auth_token;
    }
    
    public function setPasswordAttribute($value){
        $this->attributes['password'] = \Hash::make($value);      
    }

    public function roles(){
        return $this->belongsToMany( Role::class, null ,'user_ids',
        'role_ids' );
    }

    public function getAllPermissionsAttribute(){
        return $this->getAllPermissions();
    }

    // public function setAuthIdAttribute($value){
    //     if(!$value){
    //         $this->attributes['auth_id'] = AutoIncrement::getIncrementId('CMSMaster');
    //     }
    // }

    public function getAvatarUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->avatar ? Storage::disk($driver)->url($this->avatar) : config('fitin.default_asset_image');
    }

    public function setTenant($value){
        $token = request()->bearerToken();
        $key = md5("tenant:$token");
        $expired = config('jwt.ttl') * 60;
        Cache::put($key, $value, $expired);
    }

    public function getTenantIdAttribute(){
        $token = request()->bearerToken();
        $key = md5("tenant:$token");
        return Cache::get($key);
    }
}