<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

class AutoIncrement extends Eloquent {
    
    protected $collection = 'AutoIncrement';

    protected $fillable = [
        "model",
        "seq"
    ];

    public static function getIncrementId($collection){
        $seq = \DB::getCollection('AutoIncrement')->findOneAndUpdate(
            array('model' => $collection),
            array('$inc' => array('seq' => 1)),
            array('new' => true, 'upsert' => true, 'returnDocument' => \MongoDB\Operation\FindOneAndUpdate::RETURN_DOCUMENT_AFTER)
        );

        return $seq->seq;
    }
}