<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Mongodb\Eloquent\Model;

use App\FitIn\CacheManager;
use App\Traits\CacheTrait;
use App\Helpers\Utils;

//use Jenssegers\Mongodb\Auth\User as Authenticatable;

class Tenant extends BaseModel  {
    // use //HasRoles, 
    use CacheTrait;
    const CACHE_TAG = "cms-tenant";
    const THUMB_DIR = "thumb";
    
    const ACTIVE = 1;
    const INACTIVE = 0;
    protected $collection = 'Tenants';

    protected $guarded = ['_id'];
    //protected $guard = 'api';
    //protected $guard_name = 'api';
    //protected $primaryKey = '_id';
    protected $fillable = [
        'name',
        'email',
        'active'
    ];
     
    public function scopeActive($q){
        $q->where('active',self::ACTIVE);
    }
}