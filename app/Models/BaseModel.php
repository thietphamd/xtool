<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class BaseModel extends Eloquent {

    const THUMB_DEFAULT_MIME = "jpg";
    const THUMB_SIZE = [
        'small' => 256,
        'medium' => 512,
        'standard' => 1024
    ];

    /**
     * Get full image url
     */
    public function getImageUrlAttribute()
    {
        $driver = config('general.master.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('general.default_asset_image');
    }

    public function getImageSmallUrlAttribute()
    {
        $image = $this->image;
        if(!$image){
            return config('general.default_asset_image');
        }

        $driver = config('general.master.filesystem_driver');
        $array_info = pathinfo($image);
        $file_basename = $array_info['basename'];
        $filename = $array_info['filename'];
        $dirname = $array_info['dirname'];
        $array_explode = explode('/', $dirname);
        //array_pop($array_explode);

        $base_path = implode('/', $array_explode);
        $image_path = Storage::disk($driver)->path($image);
        
        $newFileName = "$filename-small." . self::THUMB_DEFAULT_MIME;
        $newPath = "$base_path/$newFileName";
        $image_path = Storage::disk($driver)->path($newPath);
    
        if(!file_exists($image_path)){
            return $this->imageUrl;
        }
        
        return Storage::disk($driver)->url($newPath);
    }


    /**
     * Query with active status
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }

    /**
     * Update Redis Cache of instance
     */
    public function updateCache(){
        $hashkey = \RedisCache::buildHash($this);
        $key = $this->getKey();
        \RedisCache::hset($hashkey, $key, $this);
        return true;
    }

    /**
     * Generate thumbnail of uploaded image
     */
    public function makeThumb()
    {
        $sizes = self::THUMB_SIZE;
        $driver = config('general.master.filesystem_driver');
        $thumb = $this->image;
        $array_info = pathinfo($thumb);
        $file_basename = $array_info['basename'];
        $filename = $array_info['filename'];
        $dirname = $array_info['dirname'];
        $array_explode = explode('/', $dirname);
        //array_pop($array_explode);

        $base_path = implode('/', $array_explode);
        $image_path = Storage::disk($driver)->path($thumb);
        if(!file_exists($image_path)){
            return;
        }
        foreach ($sizes as $type => $size) {
           
            $oImageManager = Image::make($image_path);
            $originWidth = $oImageManager->width();
            $originHeight = $oImageManager->height();
            
            if($size >= $originWidth && $size >= $originHeight){
                continue;
            }
            $oImageManager->resize($size, $size, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            // resizeCanvas keep the origin shape of image and fill additional blank to get the wanted width-height 
            // $oImageManager->resizeCanvas($arrAttr['width'],$arrAttr['height'], 'center', false, 'FFF');
            $oImageManager->stream();
            
            $newFileName = "$filename-$type" . "." . self::THUMB_DEFAULT_MIME;

            $newPathUpload = "$base_path/$newFileName";
            Storage::disk($driver)->put($newPathUpload, $oImageManager);
            
            $dataFile = [
                'size' => Storage::disk($driver)->size($newPathUpload), 
                'fileType' => 'image',
                'model' => get_class($this),
                'key' => $this->getKey(),
                'tenant_id' => $this->tenant_id,
                'path' => $newPathUpload,
                'name' => $newFileName,
                'originName' => $file_basename
            ];
            saveFileHistory($dataFile, $driver, $this);
        }

        return $this;
    }
}