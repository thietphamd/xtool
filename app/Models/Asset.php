<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use App\Traits\CacheTrait;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Helpers\Utils;
use Illuminate\Support\Facades\Auth;

class Asset extends BaseModel {

    use CacheTrait;

    protected $collection = 'Assets';
    public const CACHE_TAG = 'asset';
    public const PENDING = "pending";
    public const DRAFT = "draft";
    public const ACTIVE = "active";
    public const ARCHIVE = "archive";
    
    const MODEL_TYPE = "asset";

    const DIR_NAME = 'asset';
    const ASSET_DIR = 'assets';

    const FBX_DIR = 'fbx';
    const AB_DIR = "ab";
    const THUMB_DIR = "thumb";
    const ZIP_DIR = 'zip';
    const WEBGL_DIR = 'webgl';
    const GLB_DIR = 'glb';

   
    
    const INDEXES = ['name', 'created_at'];


    const CONFIG_NAME = "Asset";
    const CONFIG_TYPE = "asset";
    const CONFIG_NAME_STYLEFIT = "Style Fit";
    const CONFIG_TYPE_STYLEFIT = "stylefit";

    protected $fillable = [
        "name",
        'tenant_id',
        "attributes",
        "category_id",
        "name",
        "sku",
        "brand_id",
        "image",
        "status" ,
        'extensions',
        'metadata'
    ];
    
    public $appends = ['imageUrl'];
    const DEFAULT_ASSET_ATTRIBUTES = [   
        'isCommon' => 0, 
        "putable" => 0,
        "hangable"  => 0,
        "hookable"  => 0,
        "anchor"  => null,
        "isDecor" => 0,
        "isCarpet"=> 0,
        "isLight" => 0,
        "lightType" => null,
        "canArray" => 0,
        "canScale" => 0,
        "canCook" => 0,
        "canDelete" => 0,
        "canSwap" => 0
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('withTenant', function ($builder) {
            $user = Auth::user();
            if($user){
                $builder->where('tenant_id', $user->tenant_id);
            }
        });

    }

    public function asset_requests(){
        return $this->morphMany(AssetRequest::class, 'requestable', 'model', 'key');
    }

    public function getImageUrlAttribute(){
        $driver = config('general.master.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('general.default_asset_image');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id' ,'_id');
    }

    public function brand(){
        return $this->belongsTo(Brand::class, 'brand_id' ,'_id');
    }

    public function getExtensionsAttribute($value = []){
        $driver = config('general.master.filesystem_driver');
        if(!empty($value['zip']) && is_array($value['zip'])){
            $value['zip']['url'] = $value['zip']['path'] ?  Storage::disk($driver)->url($value['zip']['path'])  : null;
        
        }else{
            $value['zip'] = [
                'path' => null,
                'revision' => null,
                'url'  => null
            ];
        }

        if(!empty($value['ab']) && is_array($value['ab'])){
            $value['ab']['url'] = $value['ab']['path'] ?  Storage::disk($driver)->url($value['ab']['path'])  : null;
        
        }else{
            $value['ab'] = [
                'path' => null,
                'revision' => null,
                'url'  => null
            ];
        }

        if(!empty($value['glb']) && is_array($value['glb'])){
            $value['glb']['url'] = $value['glb']['path'] ?  Storage::disk($driver)->url($value['glb']['path'])  : null;
        
        }else{
            $value['glb'] = [
                'path' => null,
                'revision' => null,
                'url'  => null
            ];
        }

        return $value;
    }

    public function setSkuAttribute($value){
        if($value){
            $this->attributes['sku'] = strval($value); 
        }
    }

    public static function buildListAttributes(){
        $list = self::DEFAULT_ASSET_ATTRIBUTES;
        $data = [];
        foreach($list as $key=>$val){
            switch($key){
                case "anchor":
                    $data[] = [
                        'key' => $key,
                        'type' => 'select',
                        'value' => ['floor', 'wall', 'ceiling']
                    ];
                    break;
                case "lightType": 
                    $data[] = [
                        'key' => $key,
                        'type' => 'select',
                        'value' => ["line", "spot", "point"]
                    ];
                    break;
                default:
                    $data[] = [
                        'key' => $key,
                        'type' => 'boolean',
                        'value' => null
                    ];
                    break;
            }
        }
        return $data;
    }
    
}