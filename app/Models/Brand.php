<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Traits\CacheTrait;

class Brand extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'Brands';
    protected $fillable = [
        "tenant_id",
        "name" ,
        "class",
        "status",
        "image"
    ];
    const INDEXES = ['name', 'created_at'];
    public const PENDING = "pending";
    public const DRAFT = "draft";
    public const ACTIVE = "active";
    public const ARCHIVE = "archive";

    const DIR_NAME = "brands";
    public const CACHE_TAG = 'brand';
    const THUMB_DIR = "thumb";
   
    const LIST_STATUS = [
        self::PENDING,
        self::ACTIVE,
        self::ARCHIVE,
        self::DRAFT
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('withTenant', function ($builder) {
            $user = Auth::user();
            if($user){
                $builder->where('tenant_id', $user->tenant_id);
            }
        });

    }

    public function getImageUrlAttribute()
    {
        $driver = config('general.master.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('general.default_asset_image');
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }

    public function tenant(){
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }
}