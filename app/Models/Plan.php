<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Traits\CacheTrait;

class Plan extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'Plans';
    protected $fillable = [
        "tenant_id",
        "name" ,
        "builder_id",
        "metadata",
        "status",
        "image"
    ];
    const INDEXES = ['name', 'created_at'];
    public const PENDING = "pending";
    public const DRAFT = "draft";
    public const ACTIVE = "active";
    public const ARCHIVE = "archive";

    const DIR_NAME = "plans";
    public const CACHE_TAG = 'plan';
    const THUMB_DIR = "thumb";
   
    const LIST_STATUS = [
        self::PENDING,
        self::ACTIVE,
        self::ARCHIVE,
        self::DRAFT
    ];

    const DEFAULT_METADATA = [
        "json" => []
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('withTenant', function ($builder) {
            $user = Auth::user();
            if($user){
                $builder->where('tenant_id', $user->tenant_id);
            }
        });

    }
    
    public function getImageUrlAttribute()
    {
        $driver = config('general.master.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('general.default_asset_image');
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }

    public function tenant(){
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }

    public function builder(){
        return $this->belongsTo(Builder::class, 'builder_id');
    }
}