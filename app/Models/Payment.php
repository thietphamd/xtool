<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class Payment extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'Payments';
    protected $fillable = [
        "tenant_id",
        "subscription_plan_id",
        "status",
        "price",
        "recurring_month"
        
    ];

    public const PENDING = "pending";
    public const SUCCESS = "success";
    public const FAILED = "failed";
    

    public const CACHE_TAG = 'subs_payment';

    // public static function boot()
    // {
    //     parent::boot();

    //     self::creating(function ($model) {
    //         $model->attributes['int_id'] = AutoIncrement::getIncrementId('SubscriptionPlans');
    //     });

    // }

    public function tenant(){
        return $this->belongsTo(Tenant::class, 'tenant_id', '_id');
    }
    
    public function subscription_plan(){
        return $this->belongsTo(SubscriptionPlan::class, 'subscription_plan_id', '_id');
    }
}