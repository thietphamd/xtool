<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class SubscriptionApplication extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'SubscriptionApplications';
    protected $fillable = [
        "name" ,
        "description",
        "features"
    ];
    const INDEXES = ['name'];
    //public const PENDING = "pending";
    //public const PUBLISHED = "published";
    protected $hidden = ['plan_ids'];

    public const CACHE_TAG = 'subs_application';

    public function plans() {
        return $this->belongsToMany( SubscriptionPlan::class, 'null' ,'application_ids',
        'plan_ids' );
    }
}