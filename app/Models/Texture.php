<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Traits\CacheTrait;

class Texture extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'Textures';
    protected $fillable = [
        "tenant_id",
        "name" ,
        "group_id",
        "description",
        "path"
    ];
    const INDEXES = ['name', 'created_at'];

    const DIR_NAME = "textures";
    public const CACHE_TAG = 'texture';
    const THUMB_DIR = "thumb";
   
    
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('withTenant', function ($builder) {
            $user = Auth::user();
            if($user){
                $builder->where('tenant_id', $user->tenant_id);
            }
        });

    }

    public function getUrlAttribute()
    {
        $driver = config('general.master.filesystem_driver');
        return $this->path ? Storage::disk($driver)->url($this->path) : config('general.default_asset_image');
    }

    public function tenant(){
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }
}