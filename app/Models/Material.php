<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Helpers\Utils;
use App\Traits\CacheTrait;

class Material extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'Materials';
    public const CACHE_TAG = 'material';

    const MODEL_TYPE = "material";

    public const PENDING = "pending";
    public const DRAFT = "draft";
    public const ACTIVE = "active";
    public const ARCHIVE = "archive";
   
    const CONFIG_NAME = "Material";
    const CONFIG_TYPE = "material";
    
    const INDEXES = ['name', 'created_at'];
    protected $fillable = [       
        'name', 
        'tenant_id',
        'brand_id',
        'status',
        'tags',
        'image',  
        'category_id',
        'description',    
        'extensions',
        'metadata'
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('withTenant', function ($builder) {
            $user = Auth::user();
            if($user){
                $builder->where('tenant_id', $user->tenant_id);
            }
        });

    }
    
    const DIR_NAME = "materials";
    
    const AB_DIR = "ab";
    const THUMB_DIR = "thumb";
    const ZIP_DIR = 'zip';
    const WEBGL_DIR = 'webgl';
    const GLB_DIR = 'glb';

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', '_id');
    }

    public function brand(){
        return $this->belongsTo(Brand::class, 'brand_id', '_id');
    }


    public function getImageUrlAttribute()
    {
        $driver = config('general.master.filesystem_driver');
        $result = $this->image ? Storage::disk($driver)->url($this->image) : config('general.default_asset_image');
        return $result;
    }


    public function getExtensionsAttribute($value = []){
        $driver = config('general.master.filesystem_driver');
        if(!empty($value['zip']) && is_array($value['zip'])){
            $value['zip']['url'] = $value['zip']['path'] ?  Storage::disk($driver)->url($value['zip']['path'])  : null;
        
        }else{
            $value['zip'] = [
                'path' => null,
                'revision' => null,
                'url'  => null
            ];
        }

        if(!empty($value['ab']) && is_array($value['ab'])){
            $value['ab']['url'] = $value['ab']['path'] ?  Storage::disk($driver)->url($value['ab']['path'])  : null;
        
        }else{
            $value['ab'] = [
                'path' => null,
                'revision' => null,
                'url'  => null
            ];
        }

        if(!empty($value['glb']) && is_array($value['glb'])){
            $value['glb']['url'] = $value['glb']['path'] ?  Storage::disk($driver)->url($value['glb']['path'])  : null;
        
        }else{
            $value['glb'] = [
                'path' => null,
                'revision' => null,
                'url'  => null
            ];
        }

        return $value;
    }

    public function getOwnerId(){
        if($this->ownerId){
            return $this->ownerId;
        }

        $metadata = $this->metadata ?? [];
        if(isset($metadata['user']) && isset($metadata['user']['id'])){
            return $metadata['user']['id'];
        }

        return null;
    }
}