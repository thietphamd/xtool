<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;


class SubscriptionPlanOption extends Eloquent {

    protected $fillable = [
        "recurring_month" ,
        "price"
    ];
   
    public const CACHE_TAG = 'subs_plan_option';
}