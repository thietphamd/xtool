<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class Invoice extends BaseModel {

    use CacheTrait;
    
    protected $collection = 'Invoices';
    protected $fillable = [
        "payment_id",
        "tenant_id"
    ];

    //public const PENDING = "pending";
    //public const SUCCESS = "success";
    //public const FAILED = "failed";
    

    public const CACHE_TAG = 'subs_invoices';

    // public static function boot()
    // {
    //     parent::boot();

    //     self::creating(function ($model) {
    //         $model->attributes['int_id'] = AutoIncrement::getIncrementId('SubscriptionPlans');
    //     });

    // }

    public function tenant(){
        return $this->belongsTo(Tenant::class, 'tenant_id', '_id');
    }

    public function payment(){
        return $this->belongsTo(Payment::class, 'payment_id', '_id');
    }
    
}