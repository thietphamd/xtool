<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
class VersionConfig extends Eloquent {


    protected $collection = 'VersionConfig';

    const BUILD_DIR = "system/build";
    public const CACHE_TAG = 'version';
    public const PENDING = "pending";
    public const PUBLISHED = "published";
    protected $fillable = [     
        "version",
        "path",
        "status",
        "description",
        "force_update"
    ];

   
    protected $appends = ['url'];

    public static function getVersion(){
        $configs = VersionConfig::latest()->get();
        return $configs;
    }

    public static function getAllVersion($brand = null){
        if(!$brand){
            $configs = VersionConfig::latest()->get();
        }
        else {
            $configs = VersionConfig::where('brand', $brand)->latest()->get();
        }
        return $configs;
    }

    public static function getLatestVersion($brand = null){
        if(!$brand){
            $configs = VersionConfig::whereNull('brand')->latest()->first();
        }
        else {
            $configs = VersionConfig::where('brand', $brand)->latest()->first();
        }
        
        return $configs;
    }

    public static function getLatestPublishedVersion($brand = null){
        if(!$brand){
            $configs = VersionConfig::whereNull('brand')->where('status', self::PUBLISHED)->latest()->first();
        }
        else {
            $configs = VersionConfig::where('brand', $brand)->where('status', self::PUBLISHED)->latest()->first();
        }
        
        return $configs;
    }

    public static function checkVersion($version){
        $config = VersionConfig::latest()->first();

        if(!$config){
            return true;
        }

        $old_version = $config->version;
        $compare = version_compare($version, $old_version);
        if($compare < 1){
            return false;
        }

        return true;
    }


    public function getUrlAttribute(){
        $driver = config('general.master.filesystem_driver');
        return $this->path ? Storage::disk($driver)->url($this->path) : null;
    }
}