<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use App\Traits\HasRoles;
use App\FitIn\CacheManager;
use App\Traits\CacheTrait;
use App\Helpers\Utils;

//use Jenssegers\Mongodb\Auth\User as Authenticatable;

class TenantUser extends BaseModel implements JWTSubject, AuthenticatableContract, AuthorizableContract {
    // use //HasRoles, 
    use HasRoles, Authenticatable, Authorizable, CacheTrait;

    const CACHE_TAG = "cms-tenant-user";
    const WORKSPACE_DIR = "workspace/tenant";
    const THUMB_DIR = "thumb";
    
    const ACTIVE = 1;
    const INACTIVE = 0;
    protected $collection = 'TenantUsers';

    protected $guarded = ['_id'];
    //protected $guard = 'api';
    //protected $guard_name = 'api';
    //protected $primaryKey = '_id';
    protected $fillable = [
        'tenant_id',
        'name',
        'email',
        'password',
        'auth_id',
        'active',
        'avatar'
    ];
    

    protected $hidden = [
        'password',
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('withTenant', function ($builder) {
            $user = Auth::user();
            if($user){
                $builder->where('tenant_id', $user->tenant_id);
            }
        });

        self::creating(function ($model) {
                $model->attributes['auth_id'] = AutoIncrement::getIncrementId('TenantUsers');
            });

        self::created(function ($model) {
            //   forgetCacheEditorUsersRolesPermissions();
        });

        

        self::updated(function ($model) {
            //   forgetCacheEditorUsersRolesPermissions();
        });

        self::deleted(function ($model) {
            //$model->roles()->sync([]);
            //  forgetCacheEditorUsersRolesPermissions();
        });
    }
    /**
        * Get the identifier that will be stored in the subject claim of the JWT.
        *
        * @return mixed
        */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
        * Return a key value array, containing any custom claims to be added to the JWT.
        *
        * @return array
        */
    public function getJWTCustomClaims()
    {
        return [
            'auth_token' => $this->getAuthToken()
        ];
    }

    public function setAuthToken($token){
        $this->auth_token = $token;
    }

    public function getAuthToken(){
        return $this->auth_token;
    }
    
    public function setPasswordAttribute($value){
        $this->attributes['password'] = \Hash::make($value);      
    }

    public function roles(){
        return $this->belongsToMany( Role::class, null ,'user_ids',
        'role_ids' );
    }

    public function getAllPermissionsAttribute(){
        return $this->getAllPermissions();
    }


    public function getAvatarUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->avatar ? Storage::disk($driver)->url($this->avatar) : config('fitin.default_asset_image');
    }
    
    public function scopeActive($q){
        $q->where('active',self::ACTIVE);
    }

    public function tenant(){
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }
}