<?php

namespace App\Listeners;

use App\Models\Layout;
use App\Events\RejectLayout;
use App\Models\Notification;

use App\Services\MailService;

class RejectLayoutListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $mailService;

    public function __construct(MailService $mailService)
    {
       $this->mailService = $mailService;
    }

    /**
     * Handle the event.
     *
     * @param  ApproveLayout  $event
     * @return void
     */
    public function handle(RejectLayout $event)
    {
        $layout = $event->layout;
        $input['status'] = Layout::REJECTED;
        $layout->update($input);
        $data = [
            'receiverId' => $layout->ownerId,
            'type' => 'layout.rejected',
            'meta' => [
                'key' => $layout->name
            ]
            ];
        Notification::create($data);

        if($layout->owner && !empty($layout->owner->email)){
            $email = $layout->owner->email;
            $subject = 'Reject';
            $content = "You layout {$layout->name} has been rejected.";
    
            $this->mailService->sendBasic($email, $subject, $content);
        }
       

        return $layout;
    }


}
