<?php

namespace App\Listeners;

use App\Events\RequestEvent;
use App\Models\AssetRequest;

class RequestEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Handle the event.
     *
     * @param  RequestEvent  $event
     * @return void
     */
    public function handle(RequestEvent $event)
    {
        $instance = $event->instance;
        $user = $event->user;
        
        $data = [
            "tenant_id" => $user->tenant_id,
            "model" => get_class($instance),
            "key" => $instance->getKey(),
            "requested_by" => $user->auth_id,
            "status" => AssetRequest::PENDING
        ];

        $assetRequest = AssetRequest::updateOrCreate([
            "model" => get_class($instance),
            "key" => $instance->getKey(),
            "status" => AssetRequest::PENDING
        ], $data);  
        
        return $assetRequest;
        
    }
}
