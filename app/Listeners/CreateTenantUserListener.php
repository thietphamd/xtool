<?php

namespace App\Listeners;

use App\Models\Tenant;
use App\Models\TenantUser;
use App\Events\CreateTenantUser;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repositories\TenantUserRepository;

use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTenantUserListener //implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    //public $queue = 'editor';

    //public $connection = 'redis';

    protected $tenantUserRepository;

    public function __construct(
        TenantUserRepository $tenantUserRepository 
    )
    {
       $this->tenantUserRepository = $tenantUserRepository;
    }

    /**
     * Handle the event.
     *
     * @param  CreateTenantUser  $event
     * @return void
     */
    public function handle(CreateTenantUser $event)
    {
        
        $tenant = $event->tenant;
       
        $data = [
            'tenant_id' => $tenant->_id,
            'name' => $tenant->name,
            'email' => $tenant->email,
            'password' => $tenant->password,
            'active' => TenantUser::ACTIVE
        ];
        
        $tenantUser = $this->tenantUserRepository->create($data);
        return $tenantUser;
    }

}
