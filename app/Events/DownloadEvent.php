<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DownloadEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $user, $ip, $instance, $driver, $url, $path, $file;
    public function __construct($user, $ip, $file, $driver, $path, $url, $instance)
    {   
        $this->user = $user;
        $this->ip = $ip;
        $this->file = $file;
        $this->driver = $driver;
        $this->path = $path;
        $this->url = $url;
        $this->instance = $instance;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    // public function broadcastOn()
    // {
    //     return new PrivateChannel('fitin3d');
    // }
}