<?php

namespace App\Helpers;
 
use Illuminate\Support\Facades\Route;
use App\Models\V2\Sku;
use App\Models\Ver2\Layout;
use App\Models\Ver2\Bundle;
use App\Models\Ver2\Material;
use App\Models\Ver2\ColorGroup;
use App\Models\Ver2\Object3d;
use App\Models\Ver2\Category;
use App\Models\Ver2\Project;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Utils
{

    public static function convertToArray($value){

        if(is_array($value)){
            return $value;
        }

        if(is_string($value)){
            $arrValues = explode(',',$value);

            return $arrValues;
        }

        return [$value];
    }

    public static function parseSort($strSort){
        $arrSort = explode(',',$strSort);
        $arrResult = [];

        foreach ($arrSort as $sort) {

            if(substr($sort, 0, 1) === '-'){
                $sort_type = 'ASC';
                $sort_name = substr($sort, 1);
            }
            else{
                $sort_type = 'DESC';
                $sort_name = $sort;
            }
            $arrResult[$sort_name] = $sort_type;
        }

        return $arrResult;
    }

    public static function apiResource($path, $key ,$controller){
        //Index
        Route::get($path , [
            'middleware' => "permission:$key.index",
            'as' => "$key.index",
            'uses' => "$controller@index"
        ]);
        //Store    
        Route::post($path , [
            'middleware' => "permission:$key.store",
            'as' => "$key.store",
            'uses' => "$controller@store"
        ]);
        //Show    
        Route::get("$path/{id}" , [
            'middleware' => "permission:$key.show",
            'as' => "$key.show",
            'uses' => "$controller@show"
        ]);
        //Update    
        Route::put("$path/{id}" , [
            'middleware' => "permission:$key.update",
            'as' => "$key.update",
            'uses' => "$controller@update"
        ]);
        //Destroy    
        Route::delete("$path/{id}", [
            'middleware' => "permission:$key.destroy",
            'as' => "$key.destroy",
            'uses' => "$controller@destroy"
        ]);
        
    }

    public static function getFileInfo($key_name, $platform = NULL){
        $q = Sku::where('key_name', $key_name);
        $driver = config('fitin.model3d.filesystem_driver');
        $url = NULL;

        if(!$platform){
            $q = $q->whereNotNull('prefab_path');
        }else{
            $q = $q->whereNotNull("prefab_" . $platform . "_path");
        }
        $sku = $q->first();
        if($sku){
            $path = ($platform) ? $platform . '/' . $sku->{"prefab_$platform"} :  (($sku->prefab) ?? $sku->key_name) ;
            $url = Storage::disk($driver)->url($path);    
        }
        return [
            "code" => 0,
            "success" => true,
            "data" =>  [
                "type" => $platform,
                "url" => $url,
            ],
            "message" => "Successfully getting data!"
        ];
    }

    public static function getFileInfoUrl($key_name, $platform = NULL)
    {
        $arrResult = self::getFileInfo($key_name, $platform);
        $url = !empty($arrResult['data']['url'])  ? $arrResult['data']['url'] : false;
        return $url;
    }

    public static function locationPath( ...$arr){
        if(is_array($arr[0])){
            return implode('/', $arr[0]);
        }
        return implode('/', $arr);
    }

    public static function slugifyFromModel($class , $string, $separate = '-'){
        $slug_temp = Str::slug($string, $separate);
        $slug = $slug_temp;
        $int = 1;
        while ($class->where('code', $slug)->first()){
            $slug = $slug_temp . "$separate" . "$int";
            $int++;
        }
        return $slug;
    } 

    public static function LibraryCollection($collection){
        $data = [];
        foreach ($collection as $key=>$col){
            $data[$key] = $col->transform(function($item) use ($key){
                $array = [
                    'filename' => $item->_id,
                    'name' => $item->name,
                    'key' => $item->getKey(),
                    'category' => $item->category,
                    'image' => $item->imageUrl,
                    'downloadUrl' => $item->downloadUrl,
                    'version' => $item->editorProUploadVersion
                ];

                if($key == Object3d::MODEL_TYPE){
                    $attrs = $item->getOriginal('attributes');
                    $array['isTTS'] = ( is_array($attrs) && isset($attrs['isTTS']) && $attrs['isTTS'] ) ? true : false;
                    $array['image'] = $item->editorProImageUrl;
                }

                if($key == Project::MODEL_TYPE){
                    $array = [
                        'filename' => $item->_id,
                        'name' => $item->name,
                        'key' => $item->code,
                        'image' => $item->imageUrl
                    ];
                }

                if($key == Layout::MODEL_TYPE){
                    $array['project'] = $item->project;
                    $array['display_name'] = $item->display_name;
                    $array['downloadPrefabUrl'] = $item->downloadPrefabUrl;
                }
                if($key == Bundle::MODEL_TYPE){
                    $array['data'] = $item;
                    
                }

                if($key == ColorGroup::MODEL_TYPE){
                    $array['hex'] = $item->hex;
                    $array['colors'] = $item->colors;
                }

                if($key == Material::MODEL_TYPE){
                    $array['normalMap'] = $item->normalMap;
                    $array['map'] = $item->map;
                    $array['target'] = ($item->attrs && isset($item->attrs['type'])) ? $item->attrs['type'] : null;
                }

                if($key == Category::MODEL_TYPE){
                    unset($array['category']);
                    unset($array['version']);
                    unset($array['downloadUrl']);
                }
                return $array;
            });
        }

        return response()->json([
            'code' => 0,
            'data' => $data,
            'message' => 'Success'
        ]);
    }

    public static function editorStoreRoute($name, $param = []){
        $path = route($name, $param, false);
        $url =  env('APP_EDITOR_URL') . "$path";
        return  $url;
    }

    public static function UrlReplace($string, $old, $new = null){
        if (strpos($a, $old) !== false) {
            $string = str_replace($old, $new, $string);
        }

        return $string;
    }
}
