<?php

namespace App\Observers;

use App\Models\Objects;

class ObjectsObserver
{
    /**
     * Handle the objects "created" event.
     *
     * @param  \App\Models\Objects  $objects
     * @return void
     */
    public function created(Objects $objects)
    {
        //
    }

    /**
     * Handle the objects "updated" event.
     *
     * @param  \App\Models\Objects  $objects
     * @return void
     */
    public function updated(Objects $objects)
    {
        //
    }

    /**
     * Handle the objects "deleted" event.
     *
     * @param  \App\Models\Objects  $objects
     * @return void
     */
    public function deleted(Objects $objects)
    {
        //TODO delete sku
    }

    /**
     * Handle the objects "restored" event.
     *
     * @param  \App\Models\Objects  $objects
     * @return void
     */
    public function restored(Objects $objects)
    {
        //
    }

    /**
     * Handle the objects "force deleted" event.
     *
     * @param  \App\Models\Objects  $objects
     * @return void
     */
    public function forceDeleted(Objects $objects)
    {
        //
    }
}
