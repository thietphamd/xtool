#!/usr/bin/env bash
#git reset --hard HEAD~
git pull
echo "git pull ok";

composer update 
echo "composer ok";

cp -f .env.development .env

php artisan route:clear
php artisan cache:clear
php artisan config:clear
php artisan optimize:clear
php artisan command:init:master:admin
php artisan command:init:master:system:config:class

composer dump-autoload

echo "artisan ok";

chmod -R 775 bootstrap/cache/
chmod -R 777 storage/
echo "chmod ok";
